INSERT INTO public."Configuraciones" ("Nombre","Descripcion","Valor","InsertedDate","InsertedUser","LastUpdatedDate","LastUpdatedUser") VALUES
	 ('email_notifications','Es el email desde el cual se envían las notificaciones','noreply76388376@gmail.com','2023-05-06 11:34:29.385','anonymous','2023-05-06 11:34:29.385','anonymous'),
	 ('pass_notifications','Es la contraseña del email desde el cual se envían las notificaciones','#### Sustituir ####','2023-05-06 11:34:29.422','anonymous','2023-05-06 11:34:29.422','anonymous'),
	 ('nombre_empresa','Es el nombre a mostrar de la empresa','Sepeco','2023-05-06 11:34:29.431','anonymous','2023-05-06 11:34:29.431','anonymous'),
	 ('server_notifications','Es el servidor de correo electrónico para el envío de notificaciones','smtp.gmail.com','2023-05-06 11:34:29.431','anonymous','2023-05-06 11:34:29.431','anonymous'),
	 ('port_notifications','Es el número de puerto utilizado en el servidor de correo para el envío de notificaciones','587','2023-05-06 11:34:29.431','anonymous','2023-05-06 11:34:29.431','anonymous'),
	 ('url_front','Es la url base del front end del sistema','https://localhost:4200','2023-05-06 11:34:29.431','anonymous','2023-05-06 11:34:29.431','anonymous'),
	 ('link_logo_empresa','Es es el link del logo a mostrar de la empresa','https://softtero.nyc3.digitaloceanspaces.com/softtero/acj/acj-logo','2023-05-06 11:34:29.443','anonymous','2023-05-06 11:34:29.443','anonymous'),
	 ('brevo_apiKey','Es la clave de aplicación para el envío de mails por brevo.','#### Sustituir ####','2023-05-06 11:34:29.443','anonymous','2023-05-06 11:34:29.443','anonymous');
