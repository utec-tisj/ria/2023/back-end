# Proyecto Backend RIA 2023 - Gestor de Llamados

## Tecnologías utilizadas
- EF Core
- DB PostgreSQL v 13, provider Npgsql
- Docker
- NLog
- MSTest
- Swagger, Swashbuckle

## Links de Interés
- https://www.nuget.org/packages/Npgsql.EntityFrameworkCore.PostgreSQL
- https://www.connectionstrings.com/postgresql/
- https://hub.docker.com/_/postgres
- https://programmingcsharp.com/nlog-net-core/
- https://github.com/NLog/NLog/wiki/Getting-started-with-ASP.NET-Core-6

## Iiniciar el backend
- docker-compose build
- docker-compose up
- http://localhost:5000/swagger/index.html
