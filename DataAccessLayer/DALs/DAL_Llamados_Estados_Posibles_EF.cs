﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Llamados_Estados_Posibles_EF : DAL_General_EF<LlamadoEstadoPosible, Llamados_Estados_Posibles>, IDAL_Llamados_Estados_Posibles
    {
        public DAL_Llamados_Estados_Posibles_EF(DBContext _db) : base(_db)
        {
        }

        public override string GetEntityName()
        {
            return "Llamado Estado Posible";
        }
    }
}
