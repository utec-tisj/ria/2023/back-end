﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Users_EF : IDAL_Users
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly UserManager<Users> userManager;
        private DBContext db;
        private string entityName = "User";

        public DAL_Users_EF(DBContext _db,
                           IHttpContextAccessor _httpContextAccessor,
                           UserManager<Users> _userManager)
        {
            this.db = _db;
            httpContextAccessor = _httpContextAccessor;
            userManager = _userManager;
        }

        public User GetLoggedUser()
        {
            if (httpContextAccessor == null || 
                httpContextAccessor.HttpContext == null ||
                httpContextAccessor.HttpContext.User == null || 
                httpContextAccessor.HttpContext.User.Identity == null)
                return null;

            string userName = httpContextAccessor.HttpContext.User.Identity.Name;
            Users user = userManager.FindByNameAsync(userName).Result;
            if (user == null)
                return null;

            user.Personas = db.Personas
                              .Where(x => x.Id == user.PersonaId)
                              .Include(x => x.TiposDeDocumentos).FirstOrDefault();

            return user.GetEntity(userManager, true);
        }

        public string GetLoggedUserName()
        {
            if (httpContextAccessor == null ||
                httpContextAccessor.HttpContext == null ||
                httpContextAccessor.HttpContext.User == null ||
                httpContextAccessor.HttpContext.User.Identity == null)
                return "anonymous";

            return httpContextAccessor.HttpContext.User.Identity.Name;
        }
    }
}
