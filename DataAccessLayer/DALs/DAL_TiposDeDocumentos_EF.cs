﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_TiposDeDocumentos_EF : DAL_General_EF<TipoDeDocumento, TiposDeDocumentos>, IDAL_TiposDeDocumentos
    {
        public DAL_TiposDeDocumentos_EF(DBContext _db) : base(_db)
        {
        }

        public override string GetEntityName()
        {
            return "Tipo de Documento";
        }
    }
}
