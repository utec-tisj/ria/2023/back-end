﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Tipos_De_Integrantes_EF : DAL_General_EF<TipoDeIntegrante, Tipos_De_Integrantes>, IDAL_Tipos_De_Integrantes
    {
        public DAL_Tipos_De_Integrantes_EF(DBContext _db) : base(_db)
        {
        }
        public override string GetEntityName()
        {
            return "Tipo de Integrante";
        }
    }
}
