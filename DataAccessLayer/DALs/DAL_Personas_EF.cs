﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Personas_EF : DAL_General_EF<Persona, Personas>, IDAL_Personas
    {
        public DAL_Personas_EF(DBContext _db) : base(_db)
        { 
        }

        public override string GetEntityName()
        {
            return "Persona";
        }

        public override IQueryable<Personas> SetIncludes(IQueryable<Personas> query)
        {
            return query.Include(x => x.TiposDeDocumentos);
        }

        public Persona GetXDocumento(long TipoDocumentoId, string Documento)
        {
            // Obtenemos la colección con los incluides
            var query = SetIncludes(db.Personas);
            
            // Filtramos por TipoDocumentoId y Documento y devolvemos el primer elemento
            return query.FirstOrDefault(x => x.TipoDeDocumentoId == TipoDocumentoId && 
                                        x.Documento == Documento)?
                        .GetEntity();
        }
    }
}
