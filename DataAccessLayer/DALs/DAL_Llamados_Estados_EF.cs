﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Llamados_Estados_EF : DAL_General_EF<LlamadoEstado, Llamados_Estados>, IDAL_Llamados_Estados
    {
        public DAL_Llamados_Estados_EF(DBContext _db) : base(_db)
        {
        }

        public override string GetEntityName()
        {
            return "Llamado Estado";
        }

        public override IQueryable<Llamados_Estados> SetIncludes(IQueryable<Llamados_Estados> query)
        {
            return query.Include(x => x.Llamados_Estados_Posibles);
        }
    }
}
