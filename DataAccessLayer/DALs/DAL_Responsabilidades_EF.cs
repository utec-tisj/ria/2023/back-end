﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Responsabilidades_EF : DAL_General_EF<Responsabilidad, Responsabilidades>, IDAL_Responsabilidades
    {
        public DAL_Responsabilidades_EF(DBContext _db) : base(_db)
        {
        }

        public override string GetEntityName()
        {
            return "Responsabilidad";
        }

        public override IQueryable<Responsabilidades> SetIncludes(IQueryable<Responsabilidades> query)
        {
            return query.Include(x => x.Areas);
        }
    }
}
