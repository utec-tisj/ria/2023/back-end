﻿using Castle.Components.DictionaryAdapter.Xml;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Llamados_EF : DAL_General_EF<Llamado, Llamados>, IDAL_Llamados
    {
        public DAL_Llamados_EF(DBContext _db) : base(_db)
        {
        }

        public override string GetEntityName()
        {
            return "Llamado";
        }

        public override IQueryable<Llamados> SetIncludes(IQueryable<Llamados> query)
        {
            return query.Include(x => x.Areas)
                        .Include(x => x.Llamados_Estados)
                        .ThenInclude(x => x.Llamados_Estados_Posibles)
                        .Include(x => x.Postulantes)
                        .ThenInclude(x => x.Personas)
                        .ThenInclude(x => x.TiposDeDocumentos)
                        .Include(x => x.Miembros_Tribunales)
                        .ThenInclude(x => x.Personas)
                        .ThenInclude(x => x.TiposDeDocumentos)
                        .Include(x => x.Miembros_Tribunales)
                        .ThenInclude(x => x.Tipos_De_Integrantes);
        }

        public EntityListResponseContainerDTO<Llamado> GetPagedLlamados(EntityListRequestDTO<Llamado, LlamadosFilterParamsDTO> request)
        {
            IQueryable<Llamados> query = db.Llamados;

            query = SetIncludes(query);

            query = query.Where(x => (request.Filters.Activo == null || x.Activo == request.Filters.Activo) &&
                                     (request.Filters.EstadoId <= 0 || x.Llamados_Estados.OrderByDescending(e => e.Id).First().Llamados_Estados_Posibles.Id == request.Filters.EstadoId) &&
                                     (request.Filters.PersonaTribunalId <= 0 || x.Miembros_Tribunales.Count(t => t.Personas.Id == request.Filters.PersonaTribunalId) > 0) &&
                                     (string.IsNullOrEmpty(request.Filters.Nombre) || x.Nombre.Contains(request.Filters.Nombre)) &&
                                     (string.IsNullOrEmpty(request.Filters.Identificador) || x.Identificador.Contains(request.Filters.Identificador)));

            // Obtenemos la cantidad total de elementos con los filtros aplicados
            int total = query.Count();

            // Ordenamos por Id
            query = query.OrderBy(x => x.Id);

            // Si nos pasan un offset y un limit, paginamos
            if (request.Limit != -1)
            {
                query = query.Skip(request.Offset)
                             .Take(request.Limit);
            }

            // Ejecutamos la query y mapeamos los resultados a entidades
            List<Llamado> listResult = query.Select(x => x.GetEntity())
                                            .ToList();

            // Devolvemos los resultados paginados
            EntityListResponseContainerDTO<Llamado> result = new EntityListResponseContainerDTO<Llamado>();
            result.SetValues(listResult, request.Offset, request.Limit, total);
            return result;
        }
    }
}
