﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace DataAccessLayer.DALs
{
    public abstract class DAL_General_EF<TEntity, TModel> : IDAL_General<TEntity, TModel> 
        where TEntity : class 
        where TModel : class, IDataModel<TEntity>, new()
    {
        protected readonly DBContext db;

        public DAL_General_EF(DBContext _db)
        {
            db = _db;
        }

        public abstract string GetEntityName();

        public virtual IQueryable<TModel> SetIncludes(IQueryable<TModel> query)
        {
            return query;
        }

        public TEntity Get(long id)
        {
            // Generamos la query inicial y aplicamos los includes
            var query = SetIncludes(db.Set<TModel>().AsQueryable());

            // Filtramos por Id
            query = query.Where(x => x.Id == id);

            // Ejecutamos la query y mapeamos el resultado a una entidad
            TEntity result = query.Select(x => x.GetEntity())
                                  .FirstOrDefault();

            if (result == null)
                throw new Exception($"No existe un {GetEntityName()} con Id {id}");

            return result;
        }

        public List<TEntity> GetAll()
        {
            // Generamos la query inicial y aplicamos los includes
            var query = SetIncludes(db.Set<TModel>().AsQueryable());

            // Ejecutamos la query y mapeamos los resultados a entidades
            return query.Select(x => x.GetEntity()).ToList();
        }

        public List<TEntity> GetAllActive()
        {
            // Generamos la query inicial y aplicamos los includes
            var query = SetIncludes(db.Set<TModel>().AsQueryable());

            // Ejecutamos la query y mapeamos los resultados a entidades
            return query.Where(x => x.Activo).Select(x => x.GetEntity()).ToList();
        }

        public EntityListResponseContainerDTO<TEntity> GetPaged(EntityListRequestDTO<TEntity, GeneralFilterParamsDTO> request)
        {
            // Generamos la query inicial y aplicamos los includes
            var query = SetIncludes(db.Set<TModel>().AsQueryable());

            // Filtramos las entidades por los filtros que nos pasan
            query = query.Where(x => request.Filters.Activo == null || x.Activo == request.Filters.Activo);

            // Si nos pasan un nombre y la entidad tiene una propiedad "Nombre", filtramos por nombre
            var property = typeof(TModel).GetProperty("Nombre");
            if (property != null && request.Filters.Nombre != null)
            {
                var parameter = Expression.Parameter(typeof(TModel), "x");
                var propertyAccess = Expression.Property(parameter, "Nombre");
                var constant = Expression.Constant(request.Filters.Nombre);
                var contains = Expression.Call(propertyAccess, "Contains", Type.EmptyTypes, constant);
                var lambda = Expression.Lambda<Func<TModel, bool>>(contains, parameter);
                query = query.Where(lambda);
            }

            // Obtenemos la cantidad total de elementos con los filtros aplicados
            int total = query.Count();

            // Ordenamos por Id
            query = query.OrderBy(x => x.Id);

            // Si nos pasan un offset y un limit, paginamos
            if (request.Limit != -1) 
            {
                query = query.Skip(request.Offset)
                             .Take(request.Limit);
            }

            // Ejecutamos la query y mapeamos los resultados a entidades
            List<TEntity> listResult = query.Select(x => x.GetEntity())
                                            .ToList();

            // Devolvemos los resultados paginados
            EntityListResponseContainerDTO<TEntity> result = new EntityListResponseContainerDTO<TEntity>();
            result.SetValues(listResult, request.Offset, request.Limit, total);
            return result;
        }

        public TEntity Add(TEntity x)
        {
            TModel toSave = new TModel();
            toSave.FromEntity(x);
            db.Set<TModel>().Add(toSave);
            db.SaveChanges();
            return Get(toSave.Id);
        }

        public TEntity Update(TEntity x, long id)
        {
            TModel toSave = db.Set<TModel>().Find(id);
            if (toSave == null)
                throw new Exception($"No existe un {GetEntityName()} con Id {id}");
            toSave.FromEntity(x);
            db.Update(toSave);
            db.SaveChanges();
            return Get(toSave.Id);
        }

        public void Delete(long id)
        {
            TModel? toDelete = db.Set<TModel>().Find(id);
            if (toDelete == null)
                throw new Exception($"No existe un {GetEntityName()} con Id {id}");
            db.Set<TModel>().Remove(toDelete);
            db.SaveChanges();
        }
    }
}
