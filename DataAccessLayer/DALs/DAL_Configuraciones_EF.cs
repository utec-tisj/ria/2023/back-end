﻿using DataAccessLayer.IDALs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Configuraciones_EF : IDAL_Configuraciones
    {
        private DBContext db;

        public DAL_Configuraciones_EF(DBContext _db)
        {
            db = _db;
        }

        public Configuracion Get(long id)
        {
            return db.Configuraciones
                      .Find(id)?
                      .GetEntity();
        }

        public Configuracion GetXNombre(string nombre)
        {
            var result = db.Configuraciones
                            .Where(x => x.Nombre == nombre)
                            .FirstOrDefault();

            return result == null ? null : result.GetEntity();
        }
    }
}
