﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Miembros_Tribunales_EF : DAL_General_EF<MiembroTribunal, Miembros_Tribunales>, IDAL_Miembros_Tribunales
    {
        public DAL_Miembros_Tribunales_EF(DBContext _db) : base(_db)
        {
        }

        public override string GetEntityName()
        {
            return "Miembro Tribunal";
        }

        public override IQueryable<Miembros_Tribunales> SetIncludes(IQueryable<Miembros_Tribunales> query)
        {
            return query.Include(x => x.Personas)
                        .ThenInclude(x => x.TiposDeDocumentos)
                        .Include(x => x.Tipos_De_Integrantes);
        }
    }
}
