﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Postulantes_EF : DAL_General_EF<Postulante, Postulantes>, IDAL_Postulantes
    {
        public DAL_Postulantes_EF(DBContext _db) : base(_db)
        {
        }

        public override string GetEntityName()
        {
            return "Postulante";
        }

        public override IQueryable<Postulantes> SetIncludes(IQueryable<Postulantes> query)
        {
            return query.Include(x => x.Personas)
                        .ThenInclude(x => x.TiposDeDocumentos);
        }
    }
}
