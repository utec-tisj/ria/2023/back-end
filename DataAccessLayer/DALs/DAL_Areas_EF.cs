﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public class DAL_Areas_EF : DAL_General_EF<Area, Areas>, IDAL_Areas
    {
        public DAL_Areas_EF(DBContext _db) : base(_db)
        {
        }
        public override string GetEntityName()
        {
            return "Area";
        }
    }
}
