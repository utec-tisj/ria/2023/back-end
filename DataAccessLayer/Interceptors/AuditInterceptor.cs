﻿using System;
using System.Data.Common;
using System.Threading;
using DataAccessLayer.IDALs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace DataAccessLayer.Interceptors
{

    // Este interceptor no tiene utilizada aun, se podría utiliza por ejemplo para
    // medir tiempos de las consultas.
    public class AuditInterceptor : DbCommandInterceptor
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private string loggedUser;

        public AuditInterceptor(IHttpContextAccessor _httpContextAccessor) : base()
        {
            // Asignamos el usuario logueado
            httpContextAccessor = _httpContextAccessor;
            if (httpContextAccessor == null ||
                httpContextAccessor.HttpContext == null ||
                httpContextAccessor.HttpContext.User == null ||
                httpContextAccessor.HttpContext.User.Identity == null)
                loggedUser = "anonymous";
            else
                loggedUser = httpContextAccessor.HttpContext.User.Identity.Name;
        }

        public override InterceptionResult<int> NonQueryExecuting(
            DbCommand command,
            CommandEventData eventData,
            InterceptionResult<int> result)
        {
            // SetAuditFields(command);

            return base.NonQueryExecuting(command, eventData, result);
        }

        public override async ValueTask<InterceptionResult<int>> NonQueryExecutingAsync(
            DbCommand command,
            CommandEventData eventData,
            InterceptionResult<int> result,
            CancellationToken cancellationToken = default)
        {
            // SetAuditFields(command);

            return await base.NonQueryExecutingAsync(command, eventData, result, cancellationToken);
        }

        public override InterceptionResult<DbDataReader> ReaderExecuting(
        DbCommand command,
        CommandEventData eventData,
        InterceptionResult<DbDataReader> result)
        {
            // SetAuditFields(command);

            return result;
        }

        public override ValueTask<InterceptionResult<DbDataReader>> ReaderExecutingAsync(
            DbCommand command,
            CommandEventData eventData,
            InterceptionResult<DbDataReader> result,
            CancellationToken cancellationToken = default)
        {
            // SetAuditFields(command);

            return new ValueTask<InterceptionResult<DbDataReader>>(result);
        }

        private void SetAuditFields(DbCommand command)
        {
            // Asignamos los campos de fecha/hora y usuario de última modificación
            if (command.CommandText.StartsWith("INSERT") || command.CommandText.StartsWith("UPDATE"))
            {
                DbParameter lastUpdatedDate = command.CreateParameter();
                lastUpdatedDate.ParameterName = "@LastUpdatedDate";
                lastUpdatedDate.Value = DateTime.UtcNow;
                command.Parameters.Add(lastUpdatedDate);

                DbParameter lastUpdatedUser = command.CreateParameter();
                lastUpdatedUser.ParameterName = "@LastUpdatedUser";
                lastUpdatedUser.Value = loggedUser;
                command.Parameters.Add(lastUpdatedUser);
            }

            // Asignamos los campos de fecha/hora y usuario de creación
            if (command.CommandText.StartsWith("INSERT"))
            {
                DbParameter insertedDate = command.CreateParameter();
                insertedDate.ParameterName = "@InsertedDate";
                insertedDate.Value = DateTime.UtcNow;
                command.Parameters.Add(insertedDate);

                DbParameter insertedUser = command.CreateParameter();
                insertedUser.ParameterName = "@InsertedUser";
                insertedUser.Value = loggedUser;
                command.Parameters.Add(insertedUser);
            }
        }
    }
}