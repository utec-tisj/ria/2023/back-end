﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Areas : EntitiesBase, IDataModel<Area>
    {
        public Areas() { }

        [MaxLength(128), MinLength(3), Required]
        public string Nombre { get; set; } = string.Empty;

        public Area GetEntity()
        {
            Area area = new Area();
            area = (Area)SetEntityBaseAtributes(area);
            
            area.Nombre= Nombre;
            return area;
        }

        public void FromEntity(Area area)
        {
            EntitiesBase.FromEntity(area, this);
            Nombre = area.Nombre;
        }
    }
}
