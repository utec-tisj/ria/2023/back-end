﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Tipos_De_Integrantes : EntitiesBase, IDataModel<TipoDeIntegrante>
    {
        public Tipos_De_Integrantes() { }

        [MaxLength(128), MinLength(3), Required]
        public string Nombre { get; set; } = string.Empty;
        public int Orden { get; set; } = 0;

        public TipoDeIntegrante GetEntity()
        {
            TipoDeIntegrante tipoDeIntegrante = new TipoDeIntegrante();
            tipoDeIntegrante = (TipoDeIntegrante)SetEntityBaseAtributes(tipoDeIntegrante);

            tipoDeIntegrante.Nombre = this.Nombre;
            tipoDeIntegrante.Orden = this.Orden;

            return tipoDeIntegrante;
        }

        public void FromEntity(TipoDeIntegrante tipoDeIntegrante)
        {
            EntitiesBase.FromEntity(tipoDeIntegrante, this);

            Nombre = tipoDeIntegrante.Nombre;
            Orden = tipoDeIntegrante.Orden;
        }
    }
}
