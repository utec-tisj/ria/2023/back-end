﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Miembros_Tribunales : EntitiesBase, IDataModel<MiembroTribunal>
    {
        public int Orden { get; set; }
        public bool Renuncia { get; set; }
        public string MotivoRenuncia { get; set; }

        [ForeignKey("Llamados")]
        public long LlamadoId { get; set; }
        public Llamados Llamados { get; set; }

        [ForeignKey("Personas")]
        public long PersonaId { get; set; }
        public Personas Personas { get; set; }

        [ForeignKey("Tipos_De_Integrantes")]
        public long TipoDeIntegranteId { get; set; }
        public Tipos_De_Integrantes Tipos_De_Integrantes { get; set; }

        public MiembroTribunal GetEntity()
        {
            MiembroTribunal miembroTribunal = new MiembroTribunal();
            miembroTribunal = (MiembroTribunal)SetEntityBaseAtributes(miembroTribunal);

            miembroTribunal.Orden = Orden;
            miembroTribunal.Renuncia = Renuncia;
            miembroTribunal.MotivoRenuncia = MotivoRenuncia;
            miembroTribunal.LlamadoId = LlamadoId;
            miembroTribunal.PersonaId = PersonaId;
            miembroTribunal.Persona = Personas.GetEntity();
            miembroTribunal.TipoDeIntegranteId = TipoDeIntegranteId;
            miembroTribunal.TipoDeIntegrante = Tipos_De_Integrantes.GetEntity();

            return miembroTribunal;
        }

        public void FromEntity(MiembroTribunal miembroTribunal)
        {
            EntitiesBase.FromEntity(miembroTribunal, this);

            Orden = miembroTribunal.Orden;
            Renuncia = miembroTribunal.Renuncia;
            MotivoRenuncia = miembroTribunal.MotivoRenuncia;
            LlamadoId = miembroTribunal.LlamadoId;
            PersonaId = miembroTribunal.Persona == null ? miembroTribunal.PersonaId : miembroTribunal.Persona.Id;
            TipoDeIntegranteId = miembroTribunal.TipoDeIntegrante == null ? miembroTribunal.TipoDeIntegranteId : miembroTribunal.TipoDeIntegrante.Id;
        }
    }
}
