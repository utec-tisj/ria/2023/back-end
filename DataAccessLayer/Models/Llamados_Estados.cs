﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Llamados_Estados : EntitiesBase, IDataModel<LlamadoEstado>
    {
        public string Observacion { get; set; }

        [ForeignKey("Llamados")]
        public long LlamadoId { get; set; }
        public Llamados Llamados { get; set; }

        [ForeignKey("Llamados_Estados_Posibles")]
        public long LlamadoEstadoPosibleId { get; set; }
        public Llamados_Estados_Posibles Llamados_Estados_Posibles { get; set; }

        public LlamadoEstado GetEntity()
        {
            LlamadoEstado llamadoEstado = new LlamadoEstado();
            llamadoEstado = (LlamadoEstado)SetEntityBaseAtributes(llamadoEstado);

            llamadoEstado.Observacion = Observacion;
            llamadoEstado.FechaHora = InsertedDate;
            llamadoEstado.UsuarioTransicion = InsertedUser;
            llamadoEstado.LlamadoId = LlamadoId;
            llamadoEstado.LlamadoEstadoPosibleId = LlamadoEstadoPosibleId;
            llamadoEstado.LlamadoEstadoPosible = Llamados_Estados_Posibles.GetEntity();

            return llamadoEstado;
        }

        public void FromEntity(LlamadoEstado tema)
        {
            EntitiesBase.FromEntity(tema, this);

            Observacion = tema.Observacion;
            LlamadoId = tema.LlamadoId;
            LlamadoEstadoPosibleId = tema.LlamadoEstadoPosible == null ? tema.LlamadoEstadoPosibleId : tema.LlamadoEstadoPosible.Id;
        }
    }
}
