﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Llamados_Estados_Posibles : EntitiesBase, IDataModel<LlamadoEstadoPosible>
    {
        public Llamados_Estados_Posibles() { }

        [MaxLength(128), MinLength(3), Required]
        public string Nombre { get; set; } = string.Empty;

        public LlamadoEstadoPosible GetEntity()
        {
            LlamadoEstadoPosible llamadoEstadoPosible = new LlamadoEstadoPosible();
            llamadoEstadoPosible = (LlamadoEstadoPosible)SetEntityBaseAtributes(llamadoEstadoPosible);
            
            llamadoEstadoPosible.Nombre= Nombre;

            return llamadoEstadoPosible;
        }
        public void FromEntity(LlamadoEstadoPosible llamadoEstadoPosible)
        {
            EntitiesBase.FromEntity(llamadoEstadoPosible, this);

            Nombre = llamadoEstadoPosible.Nombre;
        }
    }
}
