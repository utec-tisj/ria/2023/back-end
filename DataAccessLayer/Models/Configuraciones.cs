﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Configuracion = Shared.Entities.Configuracion;

namespace DataAccessLayer.Models
{
    public class Configuraciones : EntitiesBase
    {
        [MaxLength(256), MinLength(3), Required]
        public string Nombre { get; set; }

        [MaxLength(512), MinLength(3), Required]
        public string Descripcion { get; set; }

        [MaxLength(256), MinLength(3), Required]
        public string Valor { get; set; }

        public Configuracion GetEntity()
        {
            Configuracion configuracion = new Configuracion();
            configuracion = (Configuracion)SetEntityBaseAtributes(configuracion);

            configuracion.Nombre = Nombre;
            configuracion.Descripcion = Descripcion;
            configuracion.Valor = Valor;

            return configuracion;
        }

        public static Configuraciones FromEntity(Configuracion configuracion, Configuraciones configuraciones)
        {
            Configuraciones configuracionToSave = configuraciones == null ?
                new Configuraciones() :
                (Configuraciones)EntitiesBase.FromEntity(configuracion, configuraciones);

            configuracionToSave.Nombre = configuracion.Nombre;
            configuracionToSave.Descripcion = configuracion.Descripcion;
            configuracionToSave.Valor= configuracion.Valor;

            return configuracionToSave;
        }
    }
}
