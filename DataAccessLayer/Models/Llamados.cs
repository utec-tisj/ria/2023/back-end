﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Llamados : EntitiesBase, IDataModel<Llamado>
    {
        public Llamados() 
        { 
            Postulantes = new List<Postulantes>();
            Miembros_Tribunales = new List<Miembros_Tribunales>();
            Llamados_Estados = new List<Llamados_Estados>();
        }

        public string Identificador { get; set; }
        public string Nombre { get; set; }
        public string LinkPlanillaPuntajes { get; set; }
        public string LinkActa { get; set; }
        public int MinutosEntrevista { get; set; }
        
        [ForeignKey("Areas")]
        public long AreaId { get; set; }
        public Areas Areas { get; set; }

        public List<Postulantes> Postulantes { get; set; }

        public List<Miembros_Tribunales> Miembros_Tribunales { get; set; }

        public List<Llamados_Estados> Llamados_Estados { get; set; }

        public Llamado GetEntity()
        {
            Llamado llamado = new Llamado();
            llamado = (Llamado)SetEntityBaseAtributes(llamado);

            llamado.Identificador = Identificador;
            llamado.Nombre = Nombre;
            llamado.LinkPlanillaPuntajes = LinkPlanillaPuntajes;
            llamado.LinkActa = LinkActa;
            llamado.MinutosEntrevista = MinutosEntrevista;
            llamado.AreaId = AreaId;
            llamado.Area = Areas.GetEntity();
            llamado.Postulantes = Postulantes.Select(x => x.GetEntity()).ToList();
            llamado.MiembrosTribunal = Miembros_Tribunales.Select(x => x.GetEntity()).ToList();
            llamado.LlamadoEstados = Llamados_Estados.Select(x => x.GetEntity()).ToList();
            llamado.UltimoEstado = Llamados_Estados.LastOrDefault()?.GetEntity();

            return llamado;
        }

        public void FromEntity(Llamado llamado)
        {
            EntitiesBase.FromEntity(llamado, this);

            Identificador = llamado.Identificador;
            Nombre = llamado.Nombre;
            LinkPlanillaPuntajes = llamado.LinkPlanillaPuntajes;
            LinkActa = llamado.LinkActa;
            MinutosEntrevista = llamado.MinutosEntrevista;
            AreaId = llamado.Area == null ? llamado.AreaId : llamado.Area.Id;
        }
    }
}
