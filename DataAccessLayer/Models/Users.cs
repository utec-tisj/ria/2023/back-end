﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Models
{
    [Keyless]
    public class Users : IdentityUser
    {
        public Users() 
        {
        }

        [MaxLength, Required]
        public string Imagen { get; set; } = "";

        public bool Activo { get; set; }

        [ForeignKey("Personas")]
        public long PersonaId { get; set; }

        [Required]
        public Personas Personas { get; set; }

        public User GetEntity(UserManager<Users>? userManager, bool addRoles)
        {
            User usuario = new User
            {
                Id = Id,
                Imagen = Imagen,
                Persona = Personas.GetEntity(),
                Username = UserName?? "",
                Email = Email?? "",
                Activo = Activo
            };

            if (addRoles)
                usuario.Roles = userManager.GetRolesAsync(this).Result.ToList();

            return usuario;
        }
    }
}
