﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Models
{
    [Index(nameof(TipoDeDocumentoId), nameof(Documento), IsUnique = true, Name = "UC_DocumentoPersona")]
    public class Personas : EntitiesBase, IDataModel<Persona>
    {
        public Personas()
        {
        }

        [ForeignKey("TiposDeDocumentos")]
        public long TipoDeDocumentoId { get; set; }

        [Required]
        public TiposDeDocumentos TiposDeDocumentos { get; set; }

        [MaxLength(128), MinLength(3), Required]
        public string Documento { get; set; } = string.Empty;

        [MaxLength(128), MinLength(3), Required]
        public string PrimerNombre { get; set; } = string.Empty;

        [MaxLength(128)]
        public string SegundoNombre { get; set; } = string.Empty;

        [MaxLength(128), MinLength(3), Required]
        public string PrimerApellido { get; set; } = string.Empty;

        [MaxLength(128)]
        public string SegundoApellido { get; set; } = string.Empty;

        public Persona GetEntity()
        {
            Persona persona = (Persona)SetEntityBaseAtributes(new Persona());

            persona.TipoDeDocumento = TiposDeDocumentos.GetEntity();
            persona.Documento = Documento;
            persona.PrimerNombre = PrimerNombre;
            persona.SegundoNombre = SegundoNombre;
            persona.PrimerApellido = PrimerApellido;
            persona.SegundoApellido = SegundoApellido;
            
            return persona;
        }

        public void FromEntity(Persona persona)
        {
            EntitiesBase.FromEntity(persona, this);

            TipoDeDocumentoId = persona.TipoDeDocumento.Id;
            Documento = persona.Documento;
            PrimerNombre = persona.PrimerNombre;
            SegundoNombre = persona.SegundoNombre;
            PrimerApellido = persona.PrimerApellido;
            SegundoApellido = persona.SegundoApellido;
        }
    }
}
