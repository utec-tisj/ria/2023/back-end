﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Responsabilidades : EntitiesBase, IDataModel<Responsabilidad>
    {
        public Responsabilidades() { }

        [MaxLength(128), MinLength(3), Required]
        public string Nombre { get; set; } = string.Empty;

        [MaxLength(2048), MinLength(3), Required]
        public string Descripcion { get; set; } = string.Empty;

        [ForeignKey("Areas")]
        public long AreaId { get; set; }
        public Areas Areas { get; set; }

        public Responsabilidad GetEntity()
        {
            Responsabilidad responsabilidad = new Responsabilidad();
            responsabilidad = (Responsabilidad)SetEntityBaseAtributes(responsabilidad);

            responsabilidad.Nombre = Nombre;
            responsabilidad.Descripcion=  Descripcion;
            responsabilidad.AreaId=  AreaId;
            responsabilidad.Area = Areas.GetEntity();
            return responsabilidad;
        }
        public void FromEntity(Responsabilidad responsabilidad)
        {
            EntitiesBase.FromEntity(responsabilidad, this);
            Nombre = responsabilidad.Nombre;
            Descripcion = responsabilidad.Descripcion;
            AreaId = responsabilidad.AreaId;
        }
    }
}
