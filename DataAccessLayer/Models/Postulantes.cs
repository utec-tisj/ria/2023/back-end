﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Postulantes : EntitiesBase, IDataModel<Postulante>
    {
        public DateTime? FechaHoraEntrevista { get; set; }
        public bool EstudioMeritosRealizado { get; set; }
        public bool EntrevistaRealizada { get; set; }

        [ForeignKey("Llamados")]
        public long LlamadoId { get; set; }
        public Llamados Llamados { get; set; }

        [ForeignKey("Personas")]
        public long PersonaId { get; set; }
        public Personas Personas { get; set; }

        public Postulante GetEntity()
        {
            Postulante postulante = new Postulante();
            postulante = (Postulante)SetEntityBaseAtributes(postulante);

            postulante.FechaHoraEntrevista = FechaHoraEntrevista;
            postulante.EstudioMeritosRealizado = EstudioMeritosRealizado;
            postulante.EntrevistaRealizada = EntrevistaRealizada;
            postulante.LlamadoId = LlamadoId;
            postulante.PersonaId = PersonaId;
            postulante.Persona = Personas.GetEntity();

            return postulante;
        }

        public void FromEntity(Postulante postulante)
        {
            EntitiesBase.FromEntity(postulante, this);

            FechaHoraEntrevista = postulante.FechaHoraEntrevista;
            EstudioMeritosRealizado = postulante.EstudioMeritosRealizado;
            EntrevistaRealizada = postulante.EntrevistaRealizada;
            LlamadoId = postulante.LlamadoId;
            PersonaId = postulante.Persona == null ? postulante.PersonaId : postulante.Persona.Id;
        }
    }
}
