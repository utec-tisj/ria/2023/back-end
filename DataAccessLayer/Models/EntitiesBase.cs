﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace DataAccessLayer.Models
{
    [Index(nameof(Activo), IsUnique = false)]
    public class EntitiesBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }

        [Required]
        public DateTime InsertedDate { get; set; } = DateTime.UtcNow;

        [Required]
        public string InsertedUser { get; set; } = "anonymous";

        [Required]
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;

        [Required]
        public string LastUpdatedUser { get; set; } = "anonymous";

        public bool Activo { get; set; } = true;

        public EntityBase SetEntityBaseAtributes(EntityBase entitiesBase)
        {
            entitiesBase.Id = Id;
            entitiesBase.InsertedUser = InsertedUser;
            entitiesBase.InsertedDate = InsertedDate;
            entitiesBase.LastUpdatedUser = LastUpdatedUser;
            entitiesBase.LastUpdatedDate = LastUpdatedDate;
            entitiesBase.Activo = Activo;

            return entitiesBase;
        }

        public static EntitiesBase FromEntity(EntityBase entityBase, EntitiesBase entitiesBase)
        {
            entitiesBase.Id = entityBase.Id;
            entitiesBase.InsertedUser = entityBase.InsertedUser;
            entitiesBase.InsertedDate = entityBase.InsertedDate;
            entitiesBase.LastUpdatedUser = entityBase.LastUpdatedUser;
            entitiesBase.LastUpdatedDate = entityBase.LastUpdatedDate;
            entitiesBase.Activo = entityBase.Activo;

            return entitiesBase;
        }
    }
}
