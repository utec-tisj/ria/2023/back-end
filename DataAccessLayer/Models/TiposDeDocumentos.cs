﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class TiposDeDocumentos : EntitiesBase, IDataModel<TipoDeDocumento>
    {
        public TiposDeDocumentos() { }

        [MaxLength(128), MinLength(3), Required]
        public string Nombre { get; set; } = string.Empty;

        public TipoDeDocumento GetEntity()
        {
            TipoDeDocumento tipoDeDocumento = new TipoDeDocumento();
            tipoDeDocumento = (TipoDeDocumento)SetEntityBaseAtributes(tipoDeDocumento);
            
            tipoDeDocumento.Nombre= Nombre;

            return tipoDeDocumento;
        }

        public void FromEntity(TipoDeDocumento tipoDeDocumento)
        {
            EntitiesBase.FromEntity(tipoDeDocumento, this);

            Nombre = tipoDeDocumento.Nombre;
        }
    }
}
