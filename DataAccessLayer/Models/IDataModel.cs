﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public interface IDataModel<T> where T : class
    {
        long Id { get; set; }
        bool Activo { get; set; }

        T GetEntity();        
        void FromEntity(T tema);
    }
}
