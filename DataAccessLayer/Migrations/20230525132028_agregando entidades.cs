﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class agregandoentidades : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo2",
                table: "TiposDeDocumentos",
                newName: "Idx_EntitiesBase_Activo5");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo1",
                table: "Personas",
                newName: "Idx_EntitiesBase_Activo3");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo",
                table: "Configuraciones",
                newName: "Idx_EntitiesBase_Activo1");

            migrationBuilder.CreateTable(
                name: "Areas",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Llamados_Estados_Posibles",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Llamados_Estados_Posibles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tipos_De_Integrantes",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Orden = table.Column<int>(type: "integer", nullable: false),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tipos_De_Integrantes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Responsabilidades",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nombre = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Descripcion = table.Column<string>(type: "character varying(2048)", maxLength: 2048, nullable: false),
                    AreaId = table.Column<long>(type: "bigint", nullable: false),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Responsabilidades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Responsabilidades_Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "Idx_EntitiesBase_Activo",
                table: "Areas",
                column: "Activo");

            migrationBuilder.CreateIndex(
                name: "Idx_EntitiesBase_Activo2",
                table: "Llamados_Estados_Posibles",
                column: "Activo");

            migrationBuilder.CreateIndex(
                name: "Idx_EntitiesBase_Activo4",
                table: "Responsabilidades",
                column: "Activo");

            migrationBuilder.CreateIndex(
                name: "IX_Responsabilidades_AreaId",
                table: "Responsabilidades",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "Idx_EntitiesBase_Activo6",
                table: "Tipos_De_Integrantes",
                column: "Activo");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Llamados_Estados_Posibles");

            migrationBuilder.DropTable(
                name: "Responsabilidades");

            migrationBuilder.DropTable(
                name: "Tipos_De_Integrantes");

            migrationBuilder.DropTable(
                name: "Areas");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo5",
                table: "TiposDeDocumentos",
                newName: "Idx_EntitiesBase_Activo2");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo3",
                table: "Personas",
                newName: "Idx_EntitiesBase_Activo1");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo1",
                table: "Configuraciones",
                newName: "Idx_EntitiesBase_Activo");
        }
    }
}
