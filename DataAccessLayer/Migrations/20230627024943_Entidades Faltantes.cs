﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class EntidadesFaltantes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Llamados_Estados",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Observacion = table.Column<string>(type: "text", nullable: false),
                    LlamadoEstadoPosibleId = table.Column<long>(type: "bigint", nullable: false),
                    LlamadosId = table.Column<long>(type: "bigint", nullable: true),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Llamados_Estados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Llamados_Estados_Llamados_Estados_Posibles_LlamadoEstadoPos~",
                        column: x => x.LlamadoEstadoPosibleId,
                        principalTable: "Llamados_Estados_Posibles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Llamados_Estados_Llamados_LlamadosId",
                        column: x => x.LlamadosId,
                        principalTable: "Llamados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Miembros_Tribunales",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Orden = table.Column<int>(type: "integer", nullable: false),
                    Renuncia = table.Column<bool>(type: "boolean", nullable: false),
                    MotivoRenuncia = table.Column<string>(type: "text", nullable: false),
                    LlamadoId = table.Column<long>(type: "bigint", nullable: false),
                    PersonaId = table.Column<long>(type: "bigint", nullable: false),
                    TipoDeIntegranteId = table.Column<long>(type: "bigint", nullable: false),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Miembros_Tribunales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Miembros_Tribunales_Llamados_LlamadoId",
                        column: x => x.LlamadoId,
                        principalTable: "Llamados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Miembros_Tribunales_Personas_PersonaId",
                        column: x => x.PersonaId,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Miembros_Tribunales_Tipos_De_Integrantes_TipoDeIntegranteId",
                        column: x => x.TipoDeIntegranteId,
                        principalTable: "Tipos_De_Integrantes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Postulantes",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FechaHoraEntrevista = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    EstudioMeritosRealizado = table.Column<bool>(type: "boolean", nullable: false),
                    EntrevistaRealizada = table.Column<bool>(type: "boolean", nullable: false),
                    LlamadoId = table.Column<long>(type: "bigint", nullable: false),
                    PersonaId = table.Column<long>(type: "bigint", nullable: false),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Postulantes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Postulantes_Llamados_LlamadoId",
                        column: x => x.LlamadoId,
                        principalTable: "Llamados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Postulantes_Personas_PersonaId",
                        column: x => x.PersonaId,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Llamados_Estados_Activo",
                table: "Llamados_Estados",
                column: "Activo");

            migrationBuilder.CreateIndex(
                name: "IX_Llamados_Estados_LlamadoEstadoPosibleId",
                table: "Llamados_Estados",
                column: "LlamadoEstadoPosibleId");

            migrationBuilder.CreateIndex(
                name: "IX_Llamados_Estados_LlamadosId",
                table: "Llamados_Estados",
                column: "LlamadosId");

            migrationBuilder.CreateIndex(
                name: "IX_Miembros_Tribunales_Activo",
                table: "Miembros_Tribunales",
                column: "Activo");

            migrationBuilder.CreateIndex(
                name: "IX_Miembros_Tribunales_LlamadoId",
                table: "Miembros_Tribunales",
                column: "LlamadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Miembros_Tribunales_PersonaId",
                table: "Miembros_Tribunales",
                column: "PersonaId");

            migrationBuilder.CreateIndex(
                name: "IX_Miembros_Tribunales_TipoDeIntegranteId",
                table: "Miembros_Tribunales",
                column: "TipoDeIntegranteId");

            migrationBuilder.CreateIndex(
                name: "IX_Postulantes_Activo",
                table: "Postulantes",
                column: "Activo");

            migrationBuilder.CreateIndex(
                name: "IX_Postulantes_LlamadoId",
                table: "Postulantes",
                column: "LlamadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Postulantes_PersonaId",
                table: "Postulantes",
                column: "PersonaId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Llamados_Estados");

            migrationBuilder.DropTable(
                name: "Miembros_Tribunales");

            migrationBuilder.DropTable(
                name: "Postulantes");
        }
    }
}
