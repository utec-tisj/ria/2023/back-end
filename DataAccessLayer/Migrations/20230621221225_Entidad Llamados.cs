﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class EntidadLlamados : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo5",
                table: "TiposDeDocumentos",
                newName: "IX_TiposDeDocumentos_Activo");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo6",
                table: "Tipos_De_Integrantes",
                newName: "IX_Tipos_De_Integrantes_Activo");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo4",
                table: "Responsabilidades",
                newName: "IX_Responsabilidades_Activo");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo3",
                table: "Personas",
                newName: "IX_Personas_Activo");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo2",
                table: "Llamados_Estados_Posibles",
                newName: "IX_Llamados_Estados_Posibles_Activo");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo1",
                table: "Configuraciones",
                newName: "IX_Configuraciones_Activo");

            migrationBuilder.RenameIndex(
                name: "Idx_EntitiesBase_Activo",
                table: "Areas",
                newName: "IX_Areas_Activo");

            migrationBuilder.CreateTable(
                name: "Llamados",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Identificador = table.Column<string>(type: "text", nullable: false),
                    Nombre = table.Column<string>(type: "text", nullable: false),
                    LinkPlanillaPuntajes = table.Column<string>(type: "text", nullable: false),
                    LinkActa = table.Column<string>(type: "text", nullable: false),
                    MinutosEntrevista = table.Column<int>(type: "integer", nullable: false),
                    AreaId = table.Column<long>(type: "bigint", nullable: false),
                    InsertedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    InsertedUser = table.Column<string>(type: "text", nullable: false),
                    LastUpdatedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdatedUser = table.Column<string>(type: "text", nullable: false),
                    Activo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Llamados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Llamados_Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Llamados_Activo",
                table: "Llamados",
                column: "Activo");

            migrationBuilder.CreateIndex(
                name: "IX_Llamados_AreaId",
                table: "Llamados",
                column: "AreaId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Llamados");

            migrationBuilder.RenameIndex(
                name: "IX_TiposDeDocumentos_Activo",
                table: "TiposDeDocumentos",
                newName: "Idx_EntitiesBase_Activo5");

            migrationBuilder.RenameIndex(
                name: "IX_Tipos_De_Integrantes_Activo",
                table: "Tipos_De_Integrantes",
                newName: "Idx_EntitiesBase_Activo6");

            migrationBuilder.RenameIndex(
                name: "IX_Responsabilidades_Activo",
                table: "Responsabilidades",
                newName: "Idx_EntitiesBase_Activo4");

            migrationBuilder.RenameIndex(
                name: "IX_Personas_Activo",
                table: "Personas",
                newName: "Idx_EntitiesBase_Activo3");

            migrationBuilder.RenameIndex(
                name: "IX_Llamados_Estados_Posibles_Activo",
                table: "Llamados_Estados_Posibles",
                newName: "Idx_EntitiesBase_Activo2");

            migrationBuilder.RenameIndex(
                name: "IX_Configuraciones_Activo",
                table: "Configuraciones",
                newName: "Idx_EntitiesBase_Activo1");

            migrationBuilder.RenameIndex(
                name: "IX_Areas_Activo",
                table: "Areas",
                newName: "Idx_EntitiesBase_Activo");
        }
    }
}
