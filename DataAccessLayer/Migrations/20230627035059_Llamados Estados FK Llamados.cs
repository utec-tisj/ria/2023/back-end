﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class LlamadosEstadosFKLlamados : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Llamados_Estados_Llamados_LlamadosId",
                table: "Llamados_Estados");

            migrationBuilder.DropIndex(
                name: "IX_Llamados_Estados_LlamadosId",
                table: "Llamados_Estados");

            migrationBuilder.DropColumn(
                name: "LlamadosId",
                table: "Llamados_Estados");

            migrationBuilder.AddColumn<long>(
                name: "LlamadoId",
                table: "Llamados_Estados",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Llamados_Estados_LlamadoId",
                table: "Llamados_Estados",
                column: "LlamadoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Llamados_Estados_Llamados_LlamadoId",
                table: "Llamados_Estados",
                column: "LlamadoId",
                principalTable: "Llamados",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Llamados_Estados_Llamados_LlamadoId",
                table: "Llamados_Estados");

            migrationBuilder.DropIndex(
                name: "IX_Llamados_Estados_LlamadoId",
                table: "Llamados_Estados");

            migrationBuilder.DropColumn(
                name: "LlamadoId",
                table: "Llamados_Estados");

            migrationBuilder.AddColumn<long>(
                name: "LlamadosId",
                table: "Llamados_Estados",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Llamados_Estados_LlamadosId",
                table: "Llamados_Estados",
                column: "LlamadosId");

            migrationBuilder.AddForeignKey(
                name: "FK_Llamados_Estados_Llamados_LlamadosId",
                table: "Llamados_Estados",
                column: "LlamadosId",
                principalTable: "Llamados",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
