﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Interceptors;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Shared.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DBContext : IdentityDbContext<Users>
    {
        private readonly IConfiguration configuration;
        private readonly ICustomLogger customLogger;
        private readonly IHttpContextAccessor httpContextAccessor;
        private string connection = "";

        public DBContext()
        {
        }

        public DBContext(IConfiguration _configuration, ICustomLogger _customLogger, IHttpContextAccessor _httpContextAccessor)
        {
            configuration = _configuration;
            customLogger = _customLogger;
            httpContextAccessor = _httpContextAccessor;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            try
            {
                // Registramos los interceptors
                // Este interceptor no tiene utilizada aun, se podría utiliza por ejemplo para
                // medir tiempos de las consultas.
                options.AddInterceptors(new AuditInterceptor(httpContextAccessor));

                // Connect To Database
                string? DB_HOST = Environment.GetEnvironmentVariable("DB_HOST");
                string? DB_PORT = Environment.GetEnvironmentVariable("DB_PORT");
                string? DB_USER = Environment.GetEnvironmentVariable("DB_USER");
                string? DB_PASS = Environment.GetEnvironmentVariable("DB_PASS");
                string? DB_NAME = Environment.GetEnvironmentVariable("DB_NAME");
                string? COMPOSE = Environment.GetEnvironmentVariable("COMPOSE");

                if (string.IsNullOrWhiteSpace(DB_HOST))
                    if (!string.IsNullOrEmpty(COMPOSE))
                        connection = "Host=db; Port=5432; database=webapi; Username=webapi; Password=webapi";
                    else
                        connection = "Host=localhost; Port=1300; database=webapi; Username=webapi; Password=webapi;";
                else
                    connection = "Host=" + DB_HOST + "; Port=" + DB_PORT + "; Username=" +
                    DB_USER + ";Password=" + DB_PASS + ";Database=" + DB_NAME;

                options.UseNpgsql(connection);
            }
            catch (Exception ex)
            {
                customLogger.Error("Error al configurar base de datos.", ex);
                customLogger.Info(connection);
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            DateTime ahora = DateTime.UtcNow;
            string user = GetLoggedUser();

            foreach (var entry in ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Modified || e.State == EntityState.Added))
            {
                // Set values of updated user and inserted date
                if (entry.Properties.Count(x => x.Metadata.Name == "LastUpdatedDate") > 0)
                    entry.Property("LastUpdatedDate").CurrentValue = ahora;
                if (entry.Properties.Count(x => x.Metadata.Name == "LastUpdatedUser") > 0)
                    entry.Property("LastUpdatedUser").CurrentValue = user;

                // Set original value of insertred user
                if (entry.Properties.Count(x => x.Metadata.Name == "InsertedUser") > 0 &&
                    entry.Property("InsertedUser").IsModified)
                {
                    // Restablece el valor original del atributo
                    entry.Property("InsertedUser").OriginalValue = entry.Property("InsertedUser").CurrentValue;
                    // Establece el estado del atributo en Unchanged
                    entry.Property("InsertedUser").IsModified = false;
                }

                // Set original value of insertred date
                if (entry.Properties.Count(x => x.Metadata.Name == "InsertedDate") > 0 &&
                    entry.Property("InsertedDate").IsModified)
                {
                    // Restablece el valor original del atributo
                    entry.Property("InsertedDate").OriginalValue = entry.Property("InsertedDate").CurrentValue;
                    // Establece el estado del atributo en Unchanged
                    entry.Property("InsertedDate").IsModified = false;
                }
            }

            foreach (var entry in ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added))
            {
                // Set values of insertred user and inserted date
                if (entry.Properties.Count(x => x.Metadata.Name == "InsertedDate") > 0)
                    entry.Property("InsertedDate").CurrentValue = ahora;
                if (entry.Properties.Count(x => x.Metadata.Name == "InsertedUser") > 0)
                    entry.Property("InsertedUser").CurrentValue = user;
            }

            return base.SaveChanges();
        }

        /// <summary>
        /// Obtiene el nombre de usuario del usuario logueado (en caso de existir)
        /// desde el contexto http.
        /// </summary>
        /// <returns></returns>
        private string GetLoggedUser()
        {
            return (httpContextAccessor == null ||
                    httpContextAccessor.HttpContext == null ||
                    httpContextAccessor.HttpContext.User == null ||
                    httpContextAccessor.HttpContext.User.Identity == null ||
                    httpContextAccessor.HttpContext.User.Identity.Name == null) ?
                "anonymous" :
                httpContextAccessor.HttpContext.User.Identity.Name;
        }

        public T Add<T>(T entity) where T : EntitiesBase
        {

            var dbSet = Set<T>();
            dbSet.Add(entity);
            SaveChanges();
            return entity;
        }

        // DB Collections
        public DbSet<TiposDeDocumentos> TiposDeDocumentos { get; set; }
        public DbSet<Personas> Personas { get; set; }
        public DbSet<Configuraciones> Configuraciones { get; set; }
        public DbSet<Areas> Areas { get; set; }
        public DbSet<Responsabilidades> Responsabilidades { get; set; }
        public DbSet<Llamados_Estados_Posibles> Llamados_Estados_Posibles { get; set; }
        public DbSet<Tipos_De_Integrantes> Tipos_De_Integrantes { get; set; }
        public DbSet<Llamados> Llamados { get; set; }
        public DbSet<Postulantes> Postulantes { get; set; }
        public DbSet<Miembros_Tribunales> Miembros_Tribunales { get; set; }
        public DbSet<Llamados_Estados> Llamados_Estados { get; set; }
    }
}
