﻿using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDALs
{
    public interface IDAL_Llamados_Estados : IDAL_General<LlamadoEstado, Llamados_Estados>
    {
    }
}
