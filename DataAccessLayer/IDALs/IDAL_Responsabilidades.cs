﻿using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDALs
{
    public interface IDAL_Responsabilidades : IDAL_General<Responsabilidad, Responsabilidades>
    {
    }
}
