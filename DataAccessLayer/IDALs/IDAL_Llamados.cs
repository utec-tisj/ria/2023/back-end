﻿using DataAccessLayer.Models;
using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDALs
{
    public interface IDAL_Llamados : IDAL_General<Llamado, Llamados>
    {
        EntityListResponseContainerDTO<Llamado> GetPagedLlamados(EntityListRequestDTO<Llamado, LlamadosFilterParamsDTO> request);
    }
}
