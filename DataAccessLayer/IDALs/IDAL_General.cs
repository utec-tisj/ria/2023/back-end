﻿using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDALs
{
    public interface IDAL_General<TEntity, TModel> 
        where TEntity : class 
        where TModel : class
    {
        string GetEntityName();

        IQueryable<TModel> SetIncludes(IQueryable<TModel> query);

        TEntity Get(long id);

        List<TEntity> GetAll();

        List<TEntity> GetAllActive();

        EntityListResponseContainerDTO<TEntity> GetPaged(EntityListRequestDTO<TEntity, GeneralFilterParamsDTO> request);

        TEntity Add(TEntity x);

        TEntity Update(TEntity x, long id);

        void Delete(long id);
    }
}
