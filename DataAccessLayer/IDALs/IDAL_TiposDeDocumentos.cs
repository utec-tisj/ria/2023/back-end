﻿using DataAccessLayer.Models;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDALs
{
    public interface IDAL_TiposDeDocumentos : IDAL_General<TipoDeDocumento, TiposDeDocumentos>
    {
    }
}
