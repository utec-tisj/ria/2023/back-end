﻿using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDALs
{
    public interface IDAL_Tipos_De_Integrantes : IDAL_General<TipoDeIntegrante, Tipos_De_Integrantes>
    {
    }
}
