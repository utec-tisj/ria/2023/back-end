﻿using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DALs
{
    public interface IDAL_Personas : IDAL_General<Persona, Personas>
    {
        Persona GetXDocumento(long TipoDocumentoId, string Documento);
    }
}
