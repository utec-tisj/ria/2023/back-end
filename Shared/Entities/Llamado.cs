﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Entities
{
    public class Llamado : EntityBase
    {
        public Llamado() 
        { 
            Postulantes = new List<Postulante>();
            MiembrosTribunal = new List<MiembroTribunal>();
            LlamadoEstados = new List<LlamadoEstado>();
        }

        public string Identificador { get; set; }
        public string Nombre { get; set; }
        public string LinkPlanillaPuntajes { get; set; }
        public string LinkActa { get; set; }
        public int MinutosEntrevista { get; set; }
        public long AreaId { get; set; }
        public Area? Area { get; set; }
        public List<Postulante> Postulantes { get; set; }
        public List<MiembroTribunal> MiembrosTribunal { get; set; }
        public List<LlamadoEstado> LlamadoEstados { get; set; }
        public LlamadoEstado? UltimoEstado { get; set; }
    }
}
