﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace Shared.Entities
{
    public class EntityBase
    {
        public long Id { get; set; }

        [JsonIgnore]
        public DateTime InsertedDate { get; set; } = DateTime.UtcNow;

        [JsonIgnore]
        public string InsertedUser { get; set; } = "anonymous";

        [JsonIgnore]
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;

        [JsonIgnore]
        public string LastUpdatedUser { get; set; } = "anonymous";

        public bool Activo { get; set; }
    }
}
