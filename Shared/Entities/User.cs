﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Entities
{
    public class User
    {
        public string Id { get; set; }
        public string Username { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public Persona? Persona { get; set; }
        public string Imagen { get; set; } = string.Empty;
        public bool Activo { get; set; }
        public List<string> Roles { get; set; } = new List<string>();

        public bool IsUserInRoles(List<string> rolesToCheck) 
        {
            return Roles.Count(x => rolesToCheck.Count(r => r == x) > 0) > 0; 
        }
    }
}
