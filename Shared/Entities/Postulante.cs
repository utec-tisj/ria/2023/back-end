﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Entities
{
    public class Postulante : EntityBase
    {
        public DateTime? FechaHoraEntrevista { get; set; }
        public bool EstudioMeritosRealizado { get; set; }
        public bool EntrevistaRealizada { get; set; }
        public long LlamadoId { get; set; }
        public long PersonaId { get; set; }
        public Persona? Persona { get; set; }
    }
}
