﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Entities
{
    public class Configuracion : EntityBase
    {
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Valor { get; set; }
    }
}
