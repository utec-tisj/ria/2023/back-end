﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Entities
{
    public class Persona : EntityBase
    {
        public Persona()
        {
        }

        public TipoDeDocumento? TipoDeDocumento { get; set; }

        public string Documento { get; set; } = string.Empty;

        public string PrimerNombre { get; set; } = string.Empty;

        public string SegundoNombre { get; set; } = string.Empty;

        public string PrimerApellido { get; set; } = string.Empty;

        public string SegundoApellido { get; set; } = string.Empty;


        public string GetFullName()
        {
            return PrimerApellido.Trim() + (" " + SegundoApellido.Trim()).TrimEnd() + ", " +
                   PrimerNombre.Trim() + (" " + SegundoNombre.Trim()).TrimEnd();
        }
    }
}
