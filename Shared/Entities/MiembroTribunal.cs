﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Entities
{
    public class MiembroTribunal : EntityBase
    {
        public int Orden { get; set; }
        public bool Renuncia { get; set; }
        public string MotivoRenuncia { get; set; }
        public long LlamadoId { get; set; }
        public long PersonaId { get; set; }
        public Persona? Persona { get; set; }
        public long TipoDeIntegranteId { get; set; }
        public TipoDeIntegrante? TipoDeIntegrante { get; set; }
    }
}
