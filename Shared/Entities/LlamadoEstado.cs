﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Entities
{
    public class LlamadoEstado : EntityBase
    {
        public DateTime FechaHora { get; set; }
        public string UsuarioTransicion { get; set; }
        public string Observacion { get; set; }
        public long LlamadoId { get; set; }
        public long LlamadoEstadoPosibleId { get; set; }
        public LlamadoEstadoPosible? LlamadoEstadoPosible { get; set; }
    }
}
