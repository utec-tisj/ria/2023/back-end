﻿using NLog;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Logs
{
    public class CustomLogger_NLog : ICustomLogger
    {
        private Logger logger;

        public CustomLogger_NLog() 
        {
            logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        }

        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Warn(string message)
        {
            logger.Warn(message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Error(Exception exception)
        {
            logger.Error(exception);
        }

        public void Error(string message, Exception exception)
        {
            logger.Error(message);
            logger.Error(exception);
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
        }
        public void Fatal(Exception exception)
        {
            logger.Fatal(exception);
        }

        public void Fatal(string message, Exception exception)
        {
            logger.Fatal(message);
            logger.Fatal(exception);
        }
    }
}
