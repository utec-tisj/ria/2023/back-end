﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Logs
{
    public interface ICustomLogger
    {
        void Trace(string message);
        void Debug(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message);
        void Error(Exception exception);
        void Error(string message, Exception exception);
        void Fatal(string message);
        void Fatal(Exception exception);
        void Fatal(string message, Exception exception);
    }
}
