﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Exceptions
{
    public class CustomException : Exception
    {
        public CustomException(string message, Exception exception) : base(message, exception)
        {

        }

        public string GetMessage()
        {
            string message = "";
            Exception exception = InnerException;

            while (exception != null)
            {
                message += exception.Message + " ";
                exception = exception.InnerException;
            }

            return message;
        }
    }
}
