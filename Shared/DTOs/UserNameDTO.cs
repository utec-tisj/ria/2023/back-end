﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs
{
    public class UserNameDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string NombreCompleto { get; set; }
    }
}
