﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs.FilterParams
{
    public class GeneralFilterParamsDTO
    {
        public bool? Activo { get; set; } = null;
        public string Nombre { get; set; } = string.Empty;
    }
}
