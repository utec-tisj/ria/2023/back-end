﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs.FilterParams
{
    public class LlamadosFilterParamsDTO
    {
        public bool? Activo { get; set; } = null;
        public string Nombre { get; set; } = string.Empty;
        public string Identificador { get; set; }
        public long PersonaTribunalId { get; set; }
        public long EstadoId { get; set; }
    }
}
