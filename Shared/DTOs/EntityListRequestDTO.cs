﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs
{
    public class EntityListRequestDTO<T, P>
    {
        public EntityListRequestDTO()
        { 
        }

        public int Limit { get; set; }

        public int Offset { get; set; }

        public long Id { get; set; }

        public P Filters { get; set; }

        public string[] Orders { get; set; } = Array.Empty<string>();
    }
}
