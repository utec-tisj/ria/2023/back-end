﻿using Shared.Entities;

namespace Shared.DTOs
{
    public class UsuarioDTO
    {
        public string Id { get; set; } = string.Empty;

        public TipoDeDocumento? TipoDeDocumento { get; set; }

        public string Documento { get; set; } = string.Empty;

        public string PrimerNombre { get; set; } = string.Empty;

        public string SegundoNombre { get; set; } = string.Empty;

        public string PrimerApellido { get; set; } = string.Empty;

        public string SegundoApellido { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Username { get; set; } = string.Empty;

        public DateTime FechaHora { get; set; }

        public string Imagen { get; set; } = string.Empty;

        public bool Activo { get; set; }

        public string[] Roles { get; set; } = Array.Empty<string>();
    }
}
