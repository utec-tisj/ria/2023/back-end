﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs
{
    public class EntityListResponseContainerDTO<T>
    {
        public EntityListResponseContainerDTO() 
        { 
            List = new List<T>();            
        }

        public List<T> List { get; set; }

        public int TotalCount { get; set; }

        public int TotalPages { get; set; }

        public int CurrentPage { get; set; }

        public int Offset { get; set; }

        public int Limit { get; set; }

        /// <summary>
        /// Asigna los valores correspondientes a los distintos atributos.
        /// </summary>
        /// <param name="list">Lista de elementos</param>
        /// <param name="offset">Desplazamiento desde la posición 0</param>
        /// <param name="limit"></param>
        public void SetValues(List<T> list, int offset, int limit, int totalElements)
        {
            List = list;
            Offset = offset;
            Limit = limit;
            TotalCount = totalElements;

            CurrentPage = (Offset / Limit) + 1;

            // Si el límite es -1, entonces la cantidad de páginas es 1.
            // Si el total de elementos es múltiplo del límite, entonces la cantidad de
            // páginas es el cociente entre el total de elementos y el límite.
            // Si el total de elementos no es múltiplo del límite, entonces la cantidad de
            // páginas es el cociente entre el total de elementos y el límite, más 1.
            TotalPages = Limit == -1 ? 1 :
                         (TotalCount % Limit == 0 ?
                         TotalCount / Limit :
                         (TotalCount / Limit) + 1);
            
        }
    }
}
