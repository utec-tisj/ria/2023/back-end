﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs
{
    public class StatusResponseDTO
    {
        public bool Status { get; set; }

        public string StatusMessage { get; set; } = string.Empty;

        public EntityBase Entity { get; set; }
    }
}
