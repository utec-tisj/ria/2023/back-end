﻿using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace WebAPI
{
    public static class StartUp
    {
        internal static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<DBContext>())
                {
                    context?.Database.Migrate();
                }
            }
        }

        internal static async void InitializeDatabase(IApplicationBuilder app) 
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var dbContext = serviceScope.ServiceProvider.GetService<DBContext>())
                using (var userManager = serviceScope.ServiceProvider.GetService<UserManager<Users>>())
                using (var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>())
                {
                    #region Agregamos Los Roles Por defecto
                    var userRole = await roleManager.FindByIdAsync("ADMIN");
                    if (userRole == null)
                    {
                        IdentityRole identityRole = new IdentityRole();
                        identityRole = new IdentityRole();
                        identityRole.Id = "ADMIN";
                        identityRole.NormalizedName = "ADMIN";
                        identityRole.Name = "ADMIN";
                        identityRole.ConcurrencyStamp = "ADMIN";
                        await roleManager.CreateAsync(identityRole);
                    }

                    userRole = await roleManager.FindByIdAsync("TRIBUNAL");
                    if (userRole == null)
                    {
                        IdentityRole identityRole = new IdentityRole();
                        identityRole = new IdentityRole();
                        identityRole.Id = "TRIBUNAL";
                        identityRole.NormalizedName = "TRIBUNAL";
                        identityRole.Name = "TRIBUNAL";
                        identityRole.ConcurrencyStamp = "TRIBUNAL";
                        await roleManager.CreateAsync(identityRole);
                    }

                    userRole = await roleManager.FindByIdAsync("COORDINADOR");
                    if (userRole == null)
                    {
                        IdentityRole identityRole = new IdentityRole();
                        identityRole = new IdentityRole();
                        identityRole.Id = "COORDINADOR";
                        identityRole.NormalizedName = "COORDINADOR";
                        identityRole.Name = "COORDINADOR";
                        identityRole.ConcurrencyStamp = "COORDINADOR";
                        await roleManager.CreateAsync(identityRole);
                    }

                    userRole = await roleManager.FindByIdAsync("USER");
                    if (userRole == null)
                    {
                        IdentityRole identityRole = new IdentityRole();
                        identityRole = new IdentityRole();
                        identityRole.Id = "USER";
                        identityRole.NormalizedName = "USER";
                        identityRole.Name = "USER";
                        identityRole.ConcurrencyStamp = "USER";
                        await roleManager.CreateAsync(identityRole);
                    }

                    #endregion

                    #region Agregamos los tipos de documento por defecto si no existen.

                    // Agregamos los tipos de documento por defecto si no existen.
                    if (dbContext.TiposDeDocumentos.Where(x => x.Nombre == "CI").FirstOrDefault() == null)
                    {
                        TiposDeDocumentos tiposDeDocumentos = new TiposDeDocumentos()
                        {
                            Nombre = "CI"
                        };

                        dbContext.TiposDeDocumentos.Add(tiposDeDocumentos);
                        await dbContext.SaveChangesAsync();
                    }

                    if (dbContext.TiposDeDocumentos.Where(x => x.Nombre == "OTRO").FirstOrDefault() == null)
                    {
                        TiposDeDocumentos tiposDeDocumentos = new TiposDeDocumentos()
                        {
                            Nombre = "OTRO"
                        };

                        dbContext.TiposDeDocumentos.Add(tiposDeDocumentos);
                        await dbContext.SaveChangesAsync();
                    }

                    #endregion

                    #region Agregamos el usuario admin por defecto.

                    // Agregamos el usuario administrador por defecto si no exsite.
                    var userAdmin = await userManager.FindByEmailAsync("admin@admin.com");
                    if (userAdmin == null)
                    {
                        Personas persona = new Personas();
                        persona.TipoDeDocumentoId = 2;
                        persona.Documento = "XXXXXXXX";
                        persona.PrimerNombre = "Admin";
                        persona.SegundoNombre = "";
                        persona.PrimerApellido = "Admin";
                        persona.SegundoApellido = "";

                        Users user = new Users()
                        {
                            Email = "admin@admin.com",
                            SecurityStamp = Guid.NewGuid().ToString(),
                            UserName = "admin@admin.com",
                            Imagen = "",
                            Activo = true,
                        };
                        user.Personas = persona;
                        var result = await userManager.CreateAsync(user, "Abc*123!");

                        // Asignar Rol ADMIN
                        await userManager.AddToRoleAsync(user, "ADMIN");
                    }

                    #endregion
                }
            }
        }
    }
}
