﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class RegisterModel
    {
        public string Id { get; set; }

        public long TipoDocumentoId { get; set; }

        [Required(ErrorMessage = "El documento es requerido"), MinLength(3), MaxLength(128)]
        public string Documento { get; set; } = string.Empty;

        [Required(ErrorMessage = "El primer nombre es requerido"), MinLength(3), MaxLength(128)]
        public string PrimerNombre { get; set; } = string.Empty;

        public string SegundoNombre { get; set; } = string.Empty;

        [Required(ErrorMessage = "El primer apellido es requerido"), MinLength(3), MaxLength(128)]
        public string PrimerApellido { get; set; } = string.Empty;

        public string SegundoApellido { get; set; } = string.Empty;

        [EmailAddress]
        [Required(ErrorMessage = "El email es requerido"), MinLength(4), MaxLength(128)]
        public string Email { get; set; } = string.Empty;

        public string Imagen { get; set; } = string.Empty;

        public bool Activo { get; set; }
    }
}
