﻿using BusinessLayer.BLs;
using BusinessLayer.IBLs;
using DataAccessLayer;
using DataAccessLayer.DALs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog.Web;
using Shared.Logs;
using System.Text;
using WebAPI;
using WebAPI.Controllers;

var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
logger.Info("################################################################");
logger.Debug("# Init Main                                                   #");
logger.Info("################################################################");

logger.Info("Esperando 3 segundos para que la base de datos se inicie correctamente.");
System.Threading.Thread.Sleep(3000);
logger.Info("Espera finalizada");

try
{
    // Configuration Builder
    var builder = WebApplication.CreateBuilder(args);

    // Define Configuration Manager to access appsettings.json
    ConfigurationManager configuration = builder.Configuration;

    // For Entity Framework
    builder.Services.AddDbContext<DBContext>();

    // For Identity
    builder.Services.AddIdentity<Users, IdentityRole>()
        .AddEntityFrameworkStores<DBContext>()
        .AddDefaultTokenProviders();

    #region Authentication
    // Adding Authentication
    string? JWT_SECRET = Environment.GetEnvironmentVariable("JWT_SECRET");
    if (string.IsNullOrEmpty(JWT_SECRET))
        JWT_SECRET = configuration["JWT:Secret"] == null ? "JWTKEY" : configuration["JWT:Secret"];

    builder.Services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    // Adding Jwt Bearer
    .AddJwtBearer(options =>
    {
        options.SaveToken = true;
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidAudience = configuration["JWT:ValidAudience"],
            ValidIssuer = configuration["JWT:ValidIssuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT_SECRET == null ? "" : JWT_SECRET)),
            ClockSkew = TimeSpan.FromMinutes(5)
        };
    });

    // Reset token valid for 2 hours
    builder.Services.Configure<DataProtectionTokenProviderOptions>(options =>
    {
        options.TokenLifespan = TimeSpan.FromHours(2);
    });

    #endregion

    // Add services to the container.
    builder.Services.AddControllers();

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1",
            new OpenApiInfo
            {
                Title = "Web API - V1",
                Version = "v1"
            }
         );

        var filePath = Path.Combine(AppContext.BaseDirectory, "WebAPIDocumentation.xml");
        c.IncludeXmlComments(filePath);

        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
        {
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            Scheme = "Bearer",
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Description = "JWT Authorization header using the Bearer scheme."
        });

        c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
        {
        Reference = new OpenApiReference
        {
            Type = ReferenceType.SecurityScheme,
            Id = "Bearer"
        }
    },
        new string[] {}
    }});
    });

    // Add logger
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    builder.WebHost.UseUrls("http://*:80"); // Listen only on HTTP

    /********************************************************************************************************/
    /** Add Dependencies                                                                                   **/
    /********************************************************************************************************/
    #region 

    // Loggs
    builder.Services.AddTransient<ICustomLogger, CustomLogger_NLog>();

    // DALs
    builder.Services.AddTransient<IDAL_TiposDeDocumentos, DAL_TiposDeDocumentos_EF>();
    builder.Services.AddTransient<IDAL_Personas, DAL_Personas_EF>();
    builder.Services.AddTransient<IDAL_Configuraciones, DAL_Configuraciones_EF>();
    builder.Services.AddTransient<IDAL_Users, DAL_Users_EF>();
    builder.Services.AddTransient<IDAL_Areas, DAL_Areas_EF>();
    builder.Services.AddTransient<IDAL_Responsabilidades, DAL_Responsabilidades_EF>();
    builder.Services.AddTransient<IDAL_Tipos_De_Integrantes, DAL_Tipos_De_Integrantes_EF>();
    builder.Services.AddTransient<IDAL_Llamados_Estados_Posibles, DAL_Llamados_Estados_Posibles_EF>();
    builder.Services.AddTransient<IDAL_Llamados, DAL_Llamados_EF>();
    builder.Services.AddTransient<IDAL_Llamados_Estados, DAL_Llamados_Estados_EF>();
    builder.Services.AddTransient<IDAL_Postulantes, DAL_Postulantes_EF>();
    builder.Services.AddTransient<IDAL_Miembros_Tribunales, DAL_Miembros_Tribunales_EF>();

    // BLs
    builder.Services.AddTransient<IBL_Personas, BL_Personas>();
    builder.Services.AddTransient<IBL_TiposDeDocumentos, BL_TiposDeDocumentos>();
    builder.Services.AddTransient<IBL_Configuraciones, BL_Configuraciones>();
    builder.Services.AddTransient<IBL_Notificaciones, BL_Notificaciones>();
    builder.Services.AddTransient<IBL_Areas, BL_Areas>();
    builder.Services.AddTransient<IBL_Responsabilidades, BL_Responsabilidades>();
    builder.Services.AddTransient<IBL_Tipos_De_Integrantes, BL_Tipos_De_Integrantes>();
    builder.Services.AddTransient<IBL_Llamados_Estados_Posibles, BL_Llamados_Estados_Posibles>();
    builder.Services.AddTransient<IBL_Llamados, BL_Llamados>();
    builder.Services.AddTransient<IBL_Llamados_Estados, BL_Llamados_Estados>();
    builder.Services.AddTransient<IBL_Postulantes, BL_Postulantes>();
    builder.Services.AddTransient<IBL_Miembros_Tribunales, BL_Miembros_Tribunales>();

    #endregion

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    //if (app.Environment.IsDevelopment())
    //{
    app.UseSwagger();
    app.UseSwaggerUI();
    //}

    //app.UseHttpsRedirection();

    // Make sure you call this before calling app.UseMvc()
    app.UseCors(
        options => options.AllowAnyOrigin()
                          .AllowAnyMethod()
                          .AllowAnyHeader()
    );

    app.UseAuthentication();
    app.UseAuthorization();

    StartUp.UpdateDatabase(app);
    StartUp.InitializeDatabase(app);

    // Adding middleware for buffering request body
    app.UseMiddleware<NLogRequestPostedBodyMiddleware>();

    app.MapControllers();

    logger.Info("##### Static Files #####");
    foreach (string file in Directory.EnumerateFiles("/app/Statics", "*.html"))
    {
        logger.Info(file);
    }

    app.Run();
}
catch (Exception ex)
{
    logger.Info("Error al iniciar sistema.");
    logger.Error(ex);
}
