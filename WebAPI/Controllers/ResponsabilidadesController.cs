﻿using BusinessLayer.IBLs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using Shared.Logs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResponsabilidadesController : GeneralController<Responsabilidad>
    {
        public ResponsabilidadesController(IBL_Responsabilidades _bl, ICustomLogger _customLogger) : base(_bl, _customLogger) { }

        // GET api/<ResponsabilidadesController>/5
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(Responsabilidad), 200)]
        [HttpGet("{Id}")]
        public override IActionResult Get(long Id)
        {
            return base.Get(Id);
        }

        // POST api/<ResponsabilidadesController>/Paged
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<Responsabilidad>), 200)]
        [HttpPost]
        [Route("Paged")]
        public override IActionResult GetPaged([FromBody] EntityListRequestDTO<Responsabilidad, GeneralFilterParamsDTO> x)
        {
            return base.GetPaged(x);
        }

        // POST api/<ResponsabilidadesController>
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(Responsabilidad), 200)]
        [HttpPost]
        public override IActionResult Post([FromBody] Responsabilidad x)
        {
            return base.Post(x);
        }

        // PUT api/<ResponsabilidadesController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(Responsabilidad), 200)]
        [HttpPut("{Id}")]
        public override IActionResult Put(long Id, [FromBody] Responsabilidad x)
        {
            return base.Put(Id, x);
        }

        // DELETE api/<ResponsabilidadesController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusResponseDTO), 200)]
        [HttpDelete("{Id}")]
        public override IActionResult Delete(long Id)
        {
            return base.Delete(Id);
        }
    }
}
