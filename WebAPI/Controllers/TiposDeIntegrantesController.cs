﻿using BusinessLayer.IBLs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using Shared.Logs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TiposDeIntegrantesController : GeneralController<TipoDeIntegrante>
    {
        public TiposDeIntegrantesController(IBL_Tipos_De_Integrantes _bl, ICustomLogger _customLogger) : base(_bl, _customLogger) { }

        // GET api/<TiposDeIntegrantesController>/5
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(TipoDeIntegrante), 200)]
        [HttpGet("{Id}")]
        public override IActionResult Get(long Id)
        {
            return base.Get(Id);
        }

        // POST api/<TiposDeIntegrantesController>/Paged
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<TipoDeIntegrante>), 200)]
        [HttpPost]
        [Route("Paged")]
        public override IActionResult GetPaged([FromBody] EntityListRequestDTO<TipoDeIntegrante, GeneralFilterParamsDTO> x)
        {
            return base.GetPaged(x);
        }

        // POST api/<TiposDeIntegrantesController>
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(TipoDeIntegrante), 200)]
        [HttpPost]
        public override IActionResult Post([FromBody] TipoDeIntegrante x)
        {
            return base.Post(x);
        }

        // PUT api/<TiposDeIntegrantesController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(TipoDeIntegrante), 200)]
        [HttpPut("{Id}")]
        public override IActionResult Put(long Id, [FromBody] TipoDeIntegrante x)
        {
            return base.Put(Id, x);
        }

        // DELETE api/<TiposDeIntegrantesController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusResponseDTO), 200)]
        [HttpDelete("{Id}")]
        public override IActionResult Delete(long Id)
        {
            return base.Delete(Id);
        }
    }
}
