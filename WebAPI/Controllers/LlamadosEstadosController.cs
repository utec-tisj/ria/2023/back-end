﻿using BusinessLayer.IBLs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using Shared.Logs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LlamadosEstadosController : GeneralController<LlamadoEstado>
    {
        public LlamadosEstadosController(IBL_Llamados_Estados _bl, ICustomLogger _customLogger) : base(_bl, _customLogger) { }

        // GET api/<LlamadosEstadosController>/5
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(LlamadoEstado), 200)]
        [HttpGet("{Id}")]
        [NonAction]
        public override IActionResult Get(long Id)
        {
            return base.Get(Id);
        }

        // POST api/<LlamadosEstadosController>/Paged
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<LlamadoEstado>), 200)]
        [HttpPost]
        [Route("Paged")]
        [NonAction]
        public override IActionResult GetPaged([FromBody] EntityListRequestDTO<LlamadoEstado, GeneralFilterParamsDTO> x)
        {
            return base.GetPaged(x);
        }

        // POST api/<LlamadosEstadosController>
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(LlamadoEstado), 200)]
        [HttpPost]
        public override IActionResult Post([FromBody] LlamadoEstado x)
        {
            return base.Post(x);
        }

        // PUT api/<LlamadosEstadosController>/5
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(LlamadoEstado), 200)]
        [HttpPut("{Id}")]
        [NonAction]
        public override IActionResult Put(long Id, [FromBody] LlamadoEstado x)
        {
            return base.Put(Id, x);
        }

        // DELETE api/<LlamadosEstadosController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusResponseDTO), 200)]
        [HttpDelete("{Id}")]
        [NonAction]
        public override IActionResult Delete(long Id)
        {
            return base.Delete(Id);
        }
    }
}
