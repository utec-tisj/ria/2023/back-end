﻿using BusinessLayer.BLs;
using BusinessLayer.IBLs;
using DataAccessLayer;
using DataAccessLayer.DALs;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using Shared.Exceptions;
using Shared.Logs;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Auth Controlles es el responsable de gestionar todos los endpoints correspondientes a 
    /// la autenticación del sistema.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<Users> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration configuration;
        private readonly IBL_Personas blPersonas;
        private readonly ICustomLogger customLogger;
        private readonly IBL_Notificaciones blNotificaciones;
        private readonly IBL_TiposDeDocumentos blTiposDeDocumentos;
        private readonly DBContext db;

        public AuthController(
                UserManager<Users> _userManager,
                RoleManager<IdentityRole> _roleManager,
                IConfiguration _configuration,
                IBL_Personas _blPersonas,
                ICustomLogger _customLogger,
                IBL_Notificaciones _blNotificaciones,
                IBL_TiposDeDocumentos _blTiposDeDocumentos,
                DBContext _db)
        {
            userManager = _userManager;
            roleManager = _roleManager;
            configuration = _configuration;
            blPersonas = _blPersonas;
            customLogger = _customLogger;
            blNotificaciones = _blNotificaciones;
            blTiposDeDocumentos = _blTiposDeDocumentos;
            db = _db;
        }

        [HttpPost]
        [Route("Login")]
        [ProducesResponseType(typeof(LoginResponse), 200)]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            try
            {
                Users user = await userManager.FindByNameAsync(userName: model.Username);

                if (user != null && user.Activo && await userManager.CheckPasswordAsync(user, model.Password))
                {
                    var userRoles = await userManager.GetRolesAsync(user);

                    var authClaims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(ClaimTypes.NameIdentifier, user.Id),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    };

                    foreach (var userRole in userRoles)
                    {
                        authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                    }

                    var token = GetToken(authClaims);

                    Persona persona = blPersonas.Get(user.PersonaId);

                    return Ok(new LoginResponse
                    {
                        StatusOk = true,
                        StatusMessage = "Usuario logueado correctamente!",
                        IdUsuario = user.Id,
                        Token = new JwtSecurityTokenHandler().WriteToken(token),
                        Expiration = token.ValidTo,
                        Email = user.Email,
                        ExpirationMinutes = Convert.ToInt32((token.ValidTo - DateTime.UtcNow).TotalMinutes),
                        Nombre = persona.PrimerApellido + ", " + persona.PrimerNombre,
                        Documento = persona.Documento,
                        TipoDocumento = persona.TipoDeDocumento,
                        Imagen = user.Imagen,
                        Roles = userRoles.ToList()
                    });
                }
                else
                {
                    if (user == null)
                        return Unauthorized(new LoginResponse
                        {
                            StatusOk = false,
                            StatusMessage = $"No existe un usuario con nombre de usuario {model.Username}",
                            Token = "",
                            Expiration = DateTime.MinValue,
                        });

                    if (!user.Activo)
                        return Unauthorized(new LoginResponse
                        {
                            StatusOk = false,
                            StatusMessage = $"El usuario {user.Email} no está habilitado, contacte con el administrador del sistema.",
                            Token = "",
                            Expiration = DateTime.MinValue,
                        });

                    return Unauthorized(new LoginResponse
                    {
                        StatusOk = false,
                        StatusMessage = $"Usuario o contraseña incorrecta",
                        Token = "",
                        Expiration = DateTime.MinValue,
                    });
                }
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error en Login", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpPost]
        [Route("Register")]
        [ProducesResponseType(typeof(StatusDTO), 200)]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            try
            {
                var userExists = await userManager.FindByNameAsync(model.Email);
                if (userExists != null)
                    return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, "El usuario ya existe!"));

                // Chequeamos si existe la persona.
                Persona persona = blPersonas.GetXDocumento(model.TipoDocumentoId, model.Documento);

                if (persona == null)
                {
                    persona = new Persona();
                    TipoDeDocumento tipoDeDocumento = blTiposDeDocumentos.Get(model.TipoDocumentoId);
                    if (tipoDeDocumento == null)
                        throw new Exception($"No existe un tipo de documento con id {model.TipoDocumentoId}");
                    persona.TipoDeDocumento = tipoDeDocumento;
                    persona.Documento = model.Documento;
                    persona.PrimerNombre = model.PrimerNombre;
                    persona.SegundoNombre = model.SegundoNombre;
                    persona.PrimerApellido = model.PrimerApellido;
                    persona.SegundoApellido = model.SegundoApellido;

                    persona = blPersonas.Add(persona);
                }
                else
                {
                    // Chequeamos si la persona no tiene un usuario asociado.
                    Users userAux = userManager.Users.FirstOrDefault(x => x.PersonaId == persona.Id);
                    if (userAux != null)
                        return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(
                            false, $"La persona con documento {model.TipoDocumentoId} / {model.Documento} ya tiene un usuario asociado, el mismo es {userAux.UserName}"));
                }

                // Creamos el usuario.
                Users user = new Users()
                {
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Email,
                    Imagen = model.Imagen,
                    Activo = model.Activo
                };

                user.Personas = db.Personas.Find(persona.Id);
                var result = await userManager.CreateAsync(user);

                if (!result.Succeeded)
                {
                    string errors = "";
                    result.Errors.ToList().ForEach(x => errors += x.Description + " | ");
                    return StatusCode(StatusCodes.Status500InternalServerError, new StatusDTO(false, "Error al crear usuario! Revisar los datos ingresados y probar nuevamente. Errores: " + errors));
                }

                // Asignar Rol User
                await userManager.AddToRoleAsync(user, "USER");

                // Envío notificación de activación.
                var token = await userManager.GeneratePasswordResetTokenAsync(user);
                var param = new Dictionary<string, string?>
                {
                        {"token", token },
                        {"email", user.Email }
                };
                var callback = QueryHelpers.AddQueryString(blNotificaciones.GetUrlRestorePassword(), param);
                blNotificaciones.EnviarForgotPassword(user.Email, persona.GetFullName(), callback);

                return Ok(new StatusDTO(true, "Usuario creado correctamente! Se le ha enviado un email para establecer la contraseña"));
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error al registrar usuario", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpPut]
        [Route("Users")]
        [ProducesResponseType(typeof(StatusDTO), 200)]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> UpdateUser([FromBody] RegisterModel model)
        {
            try
            {
                // Buscamos el usuario y relizamos los chequeeos.
                Users user = await userManager.FindByIdAsync(model.Id);

                if (user == null)
                    return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO
                        (false, $"No existe un usuario con email {model.Email}" ));
                if (model.Email != user.Email)
                    return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO
                        (false, $"No es posible cambiar el email del usuario" ));

                // Buscamos la persona y realizamos los chequeeos.
                Persona persona = blPersonas.GetXDocumento(model.TipoDocumentoId, model.Documento);
                TipoDeDocumento tipoDeDocumento = blTiposDeDocumentos.Get(model.TipoDocumentoId);
                if (persona != null && persona.Id != user.PersonaId)
                    return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO
                        (false, $"No es posible cambiar la persona asociada al usuario"));

                persona.PrimerNombre = model.PrimerNombre;
                persona.SegundoNombre = model.SegundoNombre;
                persona.PrimerApellido = model.PrimerApellido;
                persona.SegundoApellido = model.SegundoApellido;

                blPersonas.Update(persona, persona.Id);

                user.Imagen = model.Imagen;
                user.Activo = model.Activo;

                var result = await userManager.UpdateAsync(user);

                if (!result.Succeeded)
                {
                    string errors = "";
                    result.Errors.ToList().ForEach(x => errors += x.Description + " | ");
                    return StatusCode(StatusCodes.Status500InternalServerError, new StatusDTO(false, $"Error al actualizar usuario! Revisar los datos ingresados y probar nuevamente. Errores: {errors}"));
                }

                return Ok(new StatusDTO(true, "Usuario modificado correctamente!"));
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error al actualizar usuario", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpPost]
        [Route("Users")]
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<User>), 200)]
        public IActionResult GetUsersPaged([FromBody] EntityListRequestDTO<User, UserFilterParamsDTO> x)
        {
            try
            {
                List<User> resultAux = new List<User>();

                // Obtenemos los usuarios filtrados
                List<Users> users = userManager.Users
                    .Include(x => x.Personas)
                    .Include(x => x.Personas.TiposDeDocumentos)
                    .Where(u =>
                    (x.Filters.IdUsuario.IsNullOrEmpty() || x.Filters.IdUsuario == u.Id) &&
                    (x.Filters.Username.IsNullOrEmpty() || u.UserName.Contains(x.Filters.Username)) &&
                    (x.Filters.Email.IsNullOrEmpty() || u.Email.Contains(x.Filters.Email)) &&
                    (x.Filters.Documento.IsNullOrEmpty() || u.Personas.Documento.Contains(x.Filters.Documento)))
                    .OrderBy(x => x.Id)
                    .ToList();

                // Les agregamos la persona
                users.Skip(x.Offset).Take(x.Limit).ToList().ForEach(y =>
                {
                    User u = y.GetEntity(userManager, true);
                    u.Persona = blPersonas.Get(y.PersonaId);
                    resultAux.Add(u);
                });

                // Asignamos los valores calculados al objeto resultado
                EntityListResponseContainerDTO<User> result = new EntityListResponseContainerDTO<User>();
                result.SetValues(resultAux, x.Offset, x.Limit, users.Count());

                return Ok(result);

            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error al obtener usuarios", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpGet("Users/Basic")]
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<UserNameDTO>), 200)]
        public IActionResult GetUsersNames()
        {
            try
            {
                List<UserNameDTO> users = userManager.Users
                    .Include(x => x.Personas)
                    .Include(x => x.Personas.TiposDeDocumentos)
                    .Select(x => new UserNameDTO()
                    {
                        Id = x.Id,
                        NombreCompleto = x.Personas.GetEntity().GetFullName(),
                        UserName = x.UserName
                    }).ToList();

                return Ok(users);
            }
            catch(Exception ex)
            {
                CustomException customException = new CustomException("Error al obtener los nombres de usuarios", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpGet("Users/Roles")]
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(List<string>), 200)]
        public IActionResult GetRoles()
        {
            try
            {
                return Ok(roleManager.Roles.Select(x => x.Name).ToList());
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error al obtener roles", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpPost("Users/UserRoles")]
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusDTO), 200)]
        public IActionResult AddRole([FromBody] AddUserRoleDTO addUserRoleDTO)
        {
            try
            {
                var user = userManager.FindByIdAsync(addUserRoleDTO.UserId).Result;
                if (user == null)
                    return StatusCode(StatusCodes.Status404NotFound, new StatusDTO(false,
                        $"No existe un usuario para el id {addUserRoleDTO.UserId}"));

                var role = roleManager.FindByIdAsync(addUserRoleDTO.RoleId).Result;
                if (role == null)
                    return StatusCode(StatusCodes.Status404NotFound, new StatusDTO(false, 
                        $"No existe un rol para el id {addUserRoleDTO.RoleId}"));

                var result = userManager.AddToRoleAsync(user, role.Id).Result;

                if (result.Succeeded)
                    return Ok(new StatusDTO(true, "Role Agregado Correctamente"));
                else
                    return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, 
                        $"Error al agregar el rol al usuario. Error: {result.Errors.First()?.Description}"));
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error al agregar el rol al usuario.", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpDelete("Users/UserRoles")]
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusDTO), 200)]
        public IActionResult RemoveRole([FromBody] AddUserRoleDTO addUserRoleDTO)
        {
            try
            {
                var user = userManager.FindByIdAsync(addUserRoleDTO.UserId).Result;
                if (user == null)
                    return StatusCode(StatusCodes.Status404NotFound, new StatusDTO(false, 
                        $"No existe un usuario para el id {addUserRoleDTO.UserId}"));

                var role = roleManager.FindByIdAsync(addUserRoleDTO.RoleId).Result;
                if (role == null)
                    return StatusCode(StatusCodes.Status404NotFound, new StatusDTO(false,
                        $"No existe un rol para el id {addUserRoleDTO.RoleId}"));

                var result = userManager.RemoveFromRoleAsync(user, role.Id).Result;

                if (result.Succeeded)
                    return Ok(new StatusDTO(true, "Role removido Correctamente"));
                else
                    return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, 
                        $"Error al remover el rol al usuario. Error: {result.Errors.First()?.Description}"));
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error al remover el rol al usuario", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpPost("ForgotPassword")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(StatusDTO), 200)]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordDTO forgotPasswordDTO)
        {
            try
            {
                var user = await userManager.FindByNameAsync(forgotPasswordDTO.Email);
                if (user == null)
                    return StatusCode(StatusCodes.Status404NotFound, new StatusDTO(false, "No existe un usuario para el correo " + forgotPasswordDTO.Email));

                Persona persona = blPersonas.Get(user.PersonaId);

                var token = await userManager.GeneratePasswordResetTokenAsync(user);
                var param = new Dictionary<string, string?>
            {
                    {"token", token },
                    {"email", forgotPasswordDTO.Email }
            };
                var callback = QueryHelpers.AddQueryString(blNotificaciones.GetUrlRestorePassword(), param);

                StatusDTO result = blNotificaciones.EnviarForgotPassword(forgotPasswordDTO.Email, persona.GetFullName(), callback);

                if (!result.Status)
                    return StatusCode(StatusCodes.Status500InternalServerError, result);

                return Ok(result);
            }
            catch(Exception ex)
            {
                CustomException customException = new CustomException("Error al intentar recuperar contraseña", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        [HttpPost("ResetPassword")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(StatusDTO), 200)]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordDTO resetPasswordModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return StatusCode(StatusCodes.Status500InternalServerError, ModelState);

                var user = await userManager.FindByEmailAsync(resetPasswordModel.Email);
                if (user == null)
                    return StatusCode(StatusCodes.Status404NotFound, "No existe un usuario para el correo " + resetPasswordModel.Email);

                if (string.Compare(resetPasswordModel.Password, resetPasswordModel.ConfirmPassword) != 0)
                    return StatusCode(StatusCodes.Status400BadRequest, "La contraseña y la confirmación de la contraseña no son iguales.");

                if (string.IsNullOrEmpty(resetPasswordModel.Token))
                    return StatusCode(StatusCodes.Status400BadRequest, "Token invalido.");

                var resetPassResult = await userManager.ResetPasswordAsync(user, resetPasswordModel.Token, resetPasswordModel.Password);
                if (!resetPassResult.Succeeded)
                {
                    foreach (var error in resetPassResult.Errors)
                    {
                        ModelState.TryAddModelError(error.Code, error.Description);
                    }
                    return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
                }

                return Ok(new StatusDTO(true, ""));
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException("Error al intentar restaurar contraseña", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {
            string? JWT_SECRET = Environment.GetEnvironmentVariable("JWT_SECRET");
            if (string.IsNullOrEmpty(JWT_SECRET))
                JWT_SECRET = configuration["JWT:Secret"];

            SymmetricSecurityKey? authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWT_SECRET));

            var token = new JwtSecurityToken(
                issuer: configuration["JWT:ValidIssuer"],
                audience: configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(3),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }
    }
}