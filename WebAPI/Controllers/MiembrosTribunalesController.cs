﻿using BusinessLayer.IBLs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using Shared.Logs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MiembrosTribunalesController : GeneralController<MiembroTribunal>
    {
        public MiembrosTribunalesController(IBL_Miembros_Tribunales _bl, ICustomLogger _customLogger) : base(_bl, _customLogger) { }

        // GET api/<MiembrosTribunalesController>/5
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(MiembroTribunal), 200)]
        [HttpGet("{Id}")]
        public override IActionResult Get(long Id)
        {
            return base.Get(Id);
        }

        // POST api/<MiembrosTribunalesController>/Paged
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<MiembroTribunal>), 200)]
        [HttpPost]
        [Route("Paged")]
        public override IActionResult GetPaged([FromBody] EntityListRequestDTO<MiembroTribunal, GeneralFilterParamsDTO> x)
        {
            return base.GetPaged(x);
        }

        // POST api/<MiembrosTribunalesController>
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(MiembroTribunal), 200)]
        [HttpPost]
        public override IActionResult Post([FromBody] MiembroTribunal x)
        {
            return base.Post(x);
        }

        // PUT api/<MiembrosTribunalesController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(MiembroTribunal), 200)]
        [HttpPut("{Id}")]
        public override IActionResult Put(long Id, [FromBody] MiembroTribunal x)
        {
            return base.Put(Id, x);
        }

        // DELETE api/<MiembrosTribunalesController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusResponseDTO), 200)]
        [HttpDelete("{Id}")]
        public override IActionResult Delete(long Id)
        {
            return base.Delete(Id);
        }
    }
}
