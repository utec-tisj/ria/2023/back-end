﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using Shared.Exceptions;
using Shared.Logs;
using System.Data;
using BusinessLayer.IBLs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneralController<TEntity> : ControllerBase where TEntity : class
    {
        private readonly IBL_General<TEntity> bl;
        private readonly ICustomLogger customLogger;

        public GeneralController(IBL_General<TEntity> _bl, ICustomLogger _customLogger)
        {
            bl = _bl;
            customLogger = _customLogger;
        }

        // GET api/<GeneralController>/5
        [HttpGet("{Id}")]
        public virtual IActionResult Get(long Id)
        {
            try
            {
                return Ok(bl.Get(Id));
            }
            catch (Exception ex)
            {
                return GenerateResponse(ex, $"Error al obtener {bl.GetEntityName()}");
            }
        }

        // POST api/<GeneralController>/Paged
        [HttpPost]
        [Route("Paged")]
        public virtual IActionResult GetPaged([FromBody] EntityListRequestDTO<TEntity, GeneralFilterParamsDTO> x)
        {
            try
            {
                return Ok(bl.GetPaged(x));
            }
            catch (Exception ex)
            {
                return GenerateResponse(ex, $"Error al obtener {bl.GetEntityName()} paginado");
            }
        }

        // POST api/<GeneralController>
        [HttpPost]
        public virtual IActionResult Post([FromBody] TEntity x)
        {
            try
            {
                return Ok(bl.Add(x));
            }
            catch (Exception ex)
            {
                return GenerateResponse(ex, $"Error al guardar {bl.GetEntityName()}");
            }
        }

        // PUT api/<GeneralController>/5
        [HttpPut("{Id}")]
        public virtual IActionResult Put(long Id, [FromBody] TEntity x)
        {
            try
            {
                return Ok(bl.Update(x, Id));
            }
            catch (Exception ex)
            {
                return GenerateResponse(ex, $"Error al actualizar {bl.GetEntityName()}");
            }
        }

        // DELETE api/<GeneralController>/5
        [HttpDelete("{Id}")]
        public virtual IActionResult Delete(long Id)
        {
            try
            {
                bl.Delete(Id);
                return Ok(new StatusResponseDTO() { Status = true, StatusMessage = "" });
            }
            catch (Exception ex)
            {
                return GenerateResponse(ex, $"Error al eliminar {bl.GetEntityName()}");
            }
        }

        /// <summary>
        /// Obtiene una respuesta en caso de error combinando un mensaje y la excepción
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        public ObjectResult GenerateResponse(Exception ex, string message)
        {
            CustomException customException = new CustomException(message, ex);
            customLogger.Error(customException);
            return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
        }
    }
}
