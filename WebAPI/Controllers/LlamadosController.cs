﻿using BusinessLayer.IBLs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using Shared.Logs;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LlamadosController : GeneralController<Llamado>
    {
        private readonly IBL_Llamados bl;
        private readonly ICustomLogger logger;

        public LlamadosController(IBL_Llamados _bl, ICustomLogger _customLogger) : base(_bl, _customLogger)
        {
            bl = _bl;
            logger = _customLogger;
        }

        // GET api/<LlamadosController>/5
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(Llamado), 200)]
        [HttpGet("{Id}")]
        public override IActionResult Get(long Id)
        {
            return base.Get(Id);
        }

        // POST api/<LlamadosController>/Paged
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<Llamado>), 200)]
        [HttpPost]
        [Route("Paged")]
        [NonAction]
        public override IActionResult GetPaged([FromBody] EntityListRequestDTO<Llamado, GeneralFilterParamsDTO> x)
        {
            return base.GetPaged(x);
        }


        // POST api/<LlamadosController>/PagedLlamados
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<Llamado>), 200)]
        [HttpPost]
        [Route("Paged")]
        public IActionResult GetPagedLlamados([FromBody] EntityListRequestDTO<Llamado, LlamadosFilterParamsDTO> x)
        {
            try
            {
                return Ok(bl.GetPagedLlamados(x));
            }
            catch (Exception ex)
            {
                return GenerateResponse(ex, $"Error al obtener {bl.GetEntityName()} paginado");
            }
        }

        // POST api/<LlamadosController>
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(Llamado), 200)]
        [HttpPost]
        public override IActionResult Post([FromBody] Llamado x)
        {
            try
            {
                return Ok(bl.AddLlamado(x));
            }
            catch (Exception ex)
            {
                return GenerateResponse(ex, $"Error al guardar {bl.GetEntityName()}");
            }
        }

        // PUT api/<LlamadosController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(Llamado), 200)]
        [HttpPut("{Id}")]
        public override IActionResult Put(long Id, [FromBody] Llamado x)
        {
            return base.Put(Id, x);
        }

        // DELETE api/<LlamadosController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusResponseDTO), 200)]
        [HttpDelete("{Id}")]
        [NonAction]
        public override IActionResult Delete(long Id)
        {
            return base.Delete(Id);
        }
    }
}
