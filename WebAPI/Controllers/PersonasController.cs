﻿using BusinessLayer.IBLs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using Shared.Exceptions;
using Shared.Logs;
using System.Data;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasController : GeneralController<Persona>
    {
        private readonly IBL_Personas bl;
        private readonly ICustomLogger customLogger;

        public PersonasController(IBL_Personas _bl, ICustomLogger _customLogger) : base(_bl, _customLogger)
        {
            bl = _bl;
            customLogger = _customLogger;
        }

        // GET api/<PersonasController>/1/45293204
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(Persona), 200)]
        [HttpGet("{TipoDocumentoId}/{Documento}")]
        public IActionResult GetXDocumento(long TipoDocumentoId, string Documento)
        {
            try
            {
                return Ok(bl.GetXDocumento(TipoDocumentoId, Documento));
            }
            catch (Exception ex)
            {
                CustomException customException = new CustomException($"Error al obtener {bl.GetEntityName()} por documento", ex);
                customLogger.Error(customException);
                return StatusCode(StatusCodes.Status400BadRequest, new StatusDTO(false, customException.GetMessage()));
            }
        }

        // GET api/<PersonasController>/5
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(Persona), 200)]
        [HttpGet("{Id}")]
        public override IActionResult Get(long Id)
        {
            return base.Get(Id);
        }

        // POST api/<PersonasController>/Paged
        [Authorize(Roles = "TRIBUNAL, COORDINADOR, ADMIN")]
        [ProducesResponseType(typeof(EntityListResponseContainerDTO<Persona>), 200)]
        [HttpPost]
        [Route("Paged")]
        public override IActionResult GetPaged([FromBody] EntityListRequestDTO<Persona, GeneralFilterParamsDTO> x)
        {
            return base.GetPaged(x);
        }

        // POST api/<PersonasController>
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(Persona), 200)]
        [HttpPost]
        public override IActionResult Post([FromBody] Persona x)
        {
            return base.Post(x);
        }

        // PUT api/<PersonasController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(Persona), 200)]
        [HttpPut("{Id}")]
        public override IActionResult Put(long Id, [FromBody] Persona x)
        {
            return base.Put(Id, x);
        }

        // DELETE api/<PersonasController>/5
        [Authorize(Roles = "ADMIN")]
        [ProducesResponseType(typeof(StatusResponseDTO), 200)]
        [HttpDelete("{Id}")]
        public override IActionResult Delete(long Id)
        {
            return base.Delete(Id);
        }
    }
}
