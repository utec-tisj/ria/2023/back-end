﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Configuraciones : IBL_Configuraciones
    {
        private IDAL_Configuraciones dal;

        public BL_Configuraciones(IDAL_Configuraciones _dal)
        {
            dal = _dal;
        }

        public Configuracion Get(long id)
        {
            return dal.Get(id);
        }

        public Configuracion GetXNombre(string nombre)
        {
            return dal.GetXNombre(nombre);
        }
    }
}
