﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Responsabilidades : BL_General<Responsabilidad, Responsabilidades>, IBL_Responsabilidades
    {
        public BL_Responsabilidades(IDAL_Responsabilidades _dal) : base(_dal)
        {
        }
    }
}
