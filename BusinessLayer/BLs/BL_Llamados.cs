﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Llamados : BL_General<Llamado, Llamados>, IBL_Llamados
    {
        private readonly IDAL_Llamados dal;
        private readonly IDAL_Llamados_Estados dalEstados;

        public BL_Llamados(IDAL_Llamados _dal, IDAL_Llamados_Estados dalEstados) : base(_dal)
        {
            dal = _dal;
            this.dalEstados = dalEstados;
        }

        public EntityListResponseContainerDTO<Llamado> GetPagedLlamados(EntityListRequestDTO<Llamado, LlamadosFilterParamsDTO> request)
        {
            return dal.GetPagedLlamados(request);
        }

        public Llamado AddLlamado(Llamado x)
        {
            x = dal.Add(x);
            LlamadoEstado estado = new LlamadoEstado()
            {
                LlamadoEstadoPosibleId = 1,
                Observacion = "",
                FechaHora = x.InsertedDate,
                UsuarioTransicion = x.InsertedUser,
                LlamadoId = x.Id
            };
            dalEstados.Add(estado);
            return Get(x.Id);
        }
    }
}
