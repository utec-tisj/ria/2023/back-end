﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Postulantes : BL_General<Postulante, Postulantes>, IBL_Postulantes
    {
        public BL_Postulantes(IDAL_Postulantes _dal) : base(_dal)
        {
        }
    }
}
