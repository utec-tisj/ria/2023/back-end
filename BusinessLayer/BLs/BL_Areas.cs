﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Areas : BL_General<Area, Areas>, IBL_Areas
    {
        public BL_Areas(IDAL_Areas _dal) : base(_dal)
        {
        }
    }
}
