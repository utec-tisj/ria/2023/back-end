﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Shared.DTOs;
using Microsoft.AspNetCore.Hosting.Server;
using Shared.Logs;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Model;
using sib_api_v3_sdk.Api;

namespace BusinessLayer.BLs
{
    public class BL_Notificaciones : IBL_Notificaciones
    {
        private IDAL_Configuraciones dalConf;
        private readonly ICustomLogger customLogger;

        private string correo;
        private string password;
        private string server;
        private int port;
        private string urlFront;
        private string empresa;
        private string linkLogoEmpresa;
        private string brevoApiKey;

        public BL_Notificaciones(IDAL_Configuraciones _dalConf, ICustomLogger _customLogger)
        {
            dalConf = _dalConf;
            customLogger = _customLogger;

            try
            {
                correo = dalConf.GetXNombre("email_notifications").Valor;
                password = dalConf.GetXNombre("pass_notifications").Valor;
                server = dalConf.GetXNombre("server_notifications").Valor;
                port = int.Parse(dalConf.GetXNombre("port_notifications").Valor);
                urlFront = dalConf.GetXNombre("url_front").Valor; ;
                empresa = dalConf.GetXNombre("nombre_empresa").Valor;
                linkLogoEmpresa = dalConf.GetXNombre("link_logo_empresa").Valor;
            }
            catch (Exception ex)
            {
                customLogger.Error(ex);
            }
        }

        public string GetUrlRestorePassword()
        {
            return urlFront + "/restore-password";
        }

        public StatusDTO EnviarForgotPassword(string emailReceptor, string nombrePersona, string callback)
        {
            StatusDTO result = new StatusDTO();

            try
            {
                string file = $@"/app/Statics/ForgotPassword.html";

                using (StreamReader reader = File.OpenText(file))
                {
                    string html = reader.ReadToEnd();
                    html = html.Replace("@nombre", nombrePersona);
                    html = html.Replace("@empresa", empresa);
                    html = html.Replace("@linkLogoEmpresa", linkLogoEmpresa);
                    html = html.Replace("@callback", callback);
                    reader.Close();

                    MailMessage myMail = new MailMessage();
                    myMail.From = new MailAddress(correo);
                    myMail.To.Add(new MailAddress(emailReceptor));
                    myMail.Subject = "Restablecer Contraseña";
                    myMail.IsBodyHtml = true;
                    myMail.Body = html;

                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = server;
                    smtp.Port = port;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = true;
                    smtp.Credentials = new NetworkCredential(correo, password);
                    smtp.Send(myMail);
                }

                result.Status = true;
                result.Mensaje = "Se envió correo para restablecer contraseña a " + emailReceptor;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Mensaje = "Error al enviar correo para restablecer contraseña: " + ex.Message;
            }

            return result;
        }

        public StatusDTO EnviarForgotPasswordBrevo(string emailReceptor, string nombrePersona, string callback, int templateId)
        {
            var apiInstance = new TransactionalEmailsApi();
            if (!apiInstance.Configuration.ApiKey.ContainsKey("api-key"))
                apiInstance.Configuration.ApiKey.Add("api-key", brevoApiKey);

            var parametros = new Dictionary<string, string>();
            parametros.Add("nombre", nombrePersona);
            // ToDo cambiar logo a base 64 para que no bloqueen la imagen los proveedores de mail.
            parametros.Add("logo", linkLogoEmpresa);
            parametros.Add("urlrestaurar", callback);

            var sendSmtpEmail = new SendSmtpEmail(
                // ToDo cambiar nombre de remitente, no es el correo que se utiliza para envíar notificaciones directamente por smtp
                // Debería ser no-reply@softtero.com
                sender: new SendSmtpEmailSender("SoftTero", correo),
                to: new List<SendSmtpEmailTo> {
                new SendSmtpEmailTo(emailReceptor)
                },
                templateId: templateId,
                _params: parametros                
            );

            try
            {
                CreateSmtpEmail result = apiInstance.SendTransacEmail(sendSmtpEmail);
                return new StatusDTO(true, "");
            }
            catch (Exception ex)
            {
                return new StatusDTO(false, ex.Message);
            }
        }
    }
}
