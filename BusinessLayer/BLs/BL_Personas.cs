﻿using BusinessLayer.IBLs;
using DataAccessLayer.DALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Personas : BL_General<Persona, Personas>, IBL_Personas
    {
        private IDAL_Personas dal;

        public BL_Personas(IDAL_Personas _dal) : base(_dal)
        { 
            this.dal = _dal;
        }

        public Persona GetXDocumento(long TipoDocumentoId, string Documento)
        {
            return dal.GetXDocumento(TipoDocumentoId, Documento);
        }
    }
}
