﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_General<TEntity, TModel> : IBL_General<TEntity> 
        where TEntity : class 
        where TModel : class, IDataModel<TEntity>, new()
    {
        private readonly IDAL_General<TEntity, TModel> dal;

        public BL_General(IDAL_General<TEntity, TModel> _dal)
        {
            dal = _dal;
        }

        public string GetEntityName()
        {
            return dal.GetEntityName();
        }

        public TEntity Get(long id)
        {
            return dal.Get(id);
        }

        public List<TEntity> GetAll()
        {
            return dal.GetAll();
        }

        public EntityListResponseContainerDTO<TEntity> GetPaged(EntityListRequestDTO<TEntity, GeneralFilterParamsDTO> request)
        {
            return dal.GetPaged(request);
        }

        public TEntity Add(TEntity x)
        {
            return dal.Add(x);
        }

        public TEntity Update(TEntity x, long id)
        {
            return dal.Update(x, id);
        }

        public void Delete(long id)
        {
            dal.Delete(id);
        }
    }
}
