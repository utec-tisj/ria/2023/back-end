﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Llamados_Estados_Posibles : BL_General<LlamadoEstadoPosible, Llamados_Estados_Posibles>, IBL_Llamados_Estados_Posibles
    {
        public BL_Llamados_Estados_Posibles(IDAL_Llamados_Estados_Posibles _dal) : base(_dal)
        {
        }
    }
}
