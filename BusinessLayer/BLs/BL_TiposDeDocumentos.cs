﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_TiposDeDocumentos : BL_General<TipoDeDocumento, TiposDeDocumentos>, IBL_TiposDeDocumentos
    {
        public BL_TiposDeDocumentos(IDAL_TiposDeDocumentos _dal) : base(_dal)
        {
        }
    }
}
