﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Miembros_Tribunales : BL_General<MiembroTribunal, Miembros_Tribunales>, IBL_Miembros_Tribunales
    {
        public BL_Miembros_Tribunales(IDAL_Miembros_Tribunales _dal) : base(_dal)
        {
        }
    }
}
