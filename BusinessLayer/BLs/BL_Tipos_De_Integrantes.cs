﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Tipos_De_Integrantes : BL_General<TipoDeIntegrante, Tipos_De_Integrantes>, IBL_Tipos_De_Integrantes
    {
        public BL_Tipos_De_Integrantes(IDAL_Tipos_De_Integrantes _dal) : base(_dal)
        {
        }
    }
}
