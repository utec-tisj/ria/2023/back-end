﻿using BusinessLayer.IBLs;
using DataAccessLayer.IDALs;
using DataAccessLayer.Models;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BLs
{
    public class BL_Llamados_Estados : BL_General<LlamadoEstado, Llamados_Estados>, IBL_Llamados_Estados
    {
        public BL_Llamados_Estados(IDAL_Llamados_Estados _dal) : base(_dal)
        {
        }
    }
}
