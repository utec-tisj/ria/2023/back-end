﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.IBLs
{
    public interface IBL_Personas : IBL_General<Persona>
    {
        Persona GetXDocumento(long TipoDocumentoId, string Documento);
    }
}
