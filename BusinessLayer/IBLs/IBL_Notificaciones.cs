﻿using Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.IBLs
{
    public interface IBL_Notificaciones
    {
        string GetUrlRestorePassword();

        StatusDTO EnviarForgotPassword(string emailReceptor, string nombrePersona, string callback);

        StatusDTO EnviarForgotPasswordBrevo(string emailReceptor, string nombrePersona, string callback, int templateId);
    }
}
