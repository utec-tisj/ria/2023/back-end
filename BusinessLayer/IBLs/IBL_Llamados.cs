﻿using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.IBLs
{
    public interface IBL_Llamados : IBL_General<Llamado>
    {
        EntityListResponseContainerDTO<Llamado> GetPagedLlamados(EntityListRequestDTO<Llamado, LlamadosFilterParamsDTO> request);

        Llamado AddLlamado(Llamado x);
    }
}
