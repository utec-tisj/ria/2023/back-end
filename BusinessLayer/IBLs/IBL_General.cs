﻿using Shared.DTOs.FilterParams;
using Shared.DTOs;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.IBLs
{
    public interface IBL_General<TEntity> 
        where TEntity : class
    {
        string GetEntityName();

        TEntity Get(long id);

        List<TEntity> GetAll();

        EntityListResponseContainerDTO<TEntity> GetPaged(EntityListRequestDTO<TEntity, GeneralFilterParamsDTO> request);

        TEntity Add(TEntity x);

        TEntity Update(TEntity x, long id);

        void Delete(long id);
    }
}
