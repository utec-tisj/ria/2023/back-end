﻿using Shared.DTOs;
using Shared.DTOs.FilterParams;
using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.IBLs
{
    public interface IBL_TiposDeDocumentos : IBL_General<TipoDeDocumento>
    {
    }
}
